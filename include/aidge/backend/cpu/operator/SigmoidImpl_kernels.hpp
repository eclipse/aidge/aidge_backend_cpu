/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SIGMOIDIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_SIGMOIDIMPL_KERNELS_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/SigmoidImpl.hpp"

namespace Aidge {
template <class I, class O>
void SigmoidImpl_cpu_forward_kernel(std::size_t inputLenght,
                                    const void* input_,
                                    void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

//#pragma omp parallel for if (inputLenght > 1024)
    for (std::size_t i = 0; i < inputLenght; ++i) {
		if (input[i] > I(0)) {
			output[i] = O(1) / (O(1) + std::exp(-input[i]));
		} else {
			output[i] = std::exp(input[i]) / (O(1) + std::exp(input[i]));
		}
    }
}

template <class O, class GI, class GO>
void SigmoidImpl_cpu_backward_kernel(const std::size_t inputLenght,
                                     const void* output_, const void* grad_output_,
				     void* grad_input_) {
    const O* output = static_cast<const O*>(output_);
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);
    for (std::size_t i = 0; i < inputLenght; ++i) {
        grad_input[i] = output[i] * (O(1) - output[i]) * grad_output[i];
    }
}

// Kernels registration to implementation entry point
REGISTRAR(SigmoidImpl_cpu,
    {DataType::Float32},
    {ProdConso::inPlaceModel, Aidge::SigmoidImpl_cpu_forward_kernel<float, float>, Aidge::SigmoidImpl_cpu_backward_kernel<float, float, float>});
REGISTRAR(SigmoidImpl_cpu,
    {DataType::Float64},
    {ProdConso::inPlaceModel, Aidge::SigmoidImpl_cpu_forward_kernel<double, double>, Aidge::SigmoidImpl_cpu_backward_kernel<double, double, double>});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SIGMOIDIMPL_KERNELS_H_ */
