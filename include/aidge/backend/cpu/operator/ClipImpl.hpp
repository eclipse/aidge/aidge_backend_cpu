/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_CLIPIMPL_H_
#define AIDGE_CPU_OPERATOR_CLIPIMPL_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>
#include <algorithm>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Clip.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"


namespace Aidge {
// Operator implementation entry point for the backend
    using ClipImpl_cpu = OperatorImpl_cpu<Clip_Op,
    void(float, //Forward Types
    float, 
    const void*,
    const std::size_t, 
    void*),
    void(float,//Backward Types
    float, 
    const std::size_t,
    const void*, 
    const void*,
    void*)>;

    REGISTRAR(Clip_Op,"cpu",Aidge::ClipImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_CLIPIMPL_H_ */
