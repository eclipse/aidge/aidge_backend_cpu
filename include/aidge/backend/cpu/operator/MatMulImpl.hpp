/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_MATMULIMPL_H_
#define AIDGE_CPU_OPERATOR_MATMULIMPL_H_

#include <array>
#include <memory>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// Operator implementation entry point for the backend
using MatMulImpl_cpu = OperatorImpl_cpu<MatMul_Op,
    void(const std::size_t, const std::size_t, const std::size_t,
                              const void *, const void *, void *)>;

// Implementation entry point registration to Operator
REGISTRAR(MatMul_Op, "cpu", Aidge::MatMulImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_MATMULIMPL_H_ */
