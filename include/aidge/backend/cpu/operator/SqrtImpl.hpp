/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SQRTIMPL_H_
#define AIDGE_CPU_OPERATOR_SQRTIMPL_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Sqrt.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// Operator implementation entry point for the backend
using SqrtImpl_cpu = OperatorImpl_cpu<Sqrt_Op,
    void(const std::size_t, const void*, void*),
    void(const std::size_t, const void*, void*)>;

// Implementation entry point registration to Operator
REGISTRAR(Sqrt_Op, "cpu", Aidge::SqrtImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SQRTIMPL_H_ */
