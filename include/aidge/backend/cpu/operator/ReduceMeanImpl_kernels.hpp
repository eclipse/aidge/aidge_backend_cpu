/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_KERNELS_H_

#include <algorithm>   // std::for_each
#include <cstddef>     // std::size_t
#include <cstdint>     // std::int32_t
#include <functional>  //std::multiplies
#include <numeric>     //std::accumulate
#include <vector>

#include "aidge/backend/cpu/operator/ReduceMeanImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, T>::type
stableMean(const T* vec, size_t len, size_t stride) {
  T mean = 0;
  for (size_t i = 0; i < len; ++i) {
    mean = std::fma<T>(vec[i * stride] - mean, 1.0f / (i + 1), mean);
  }
  return mean;
}

// Specialization for integers: perform the mean computation in float
template <typename T>
typename std::enable_if<!std::is_floating_point<T>::value, T>::type
stableMean(const T* vec, size_t len, size_t stride) {
  double mean = 0;
  for (size_t i = 0; i < len; ++i) {
    mean = std::fma<double>(vec[i * stride] - mean, 1.0f / (i + 1), mean);
  }
  return mean;
}

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, T>::type
castFromFloat(T value) {
  return value;
}

template <typename T>
typename std::enable_if<!std::is_floating_point<T>::value, T>::type
castFromFloat(double value) {
  return static_cast<T>(std::nearbyint(value));
}

template <class I, class O>
void ReduceMeanImpl_cpu_forward_kernel(const std::vector<std::int32_t>& axes,
                                    DimSize_t /*keepDims*/,
                                    const std::vector<DimSize_t>& inputDims,
                                    const void* input_,
                                    void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    const std::size_t nb_dims = inputDims.size();
    const std::size_t totalElements = std::accumulate(inputDims.cbegin(), inputDims.cend(), 1, std::multiplies<std::size_t>());

    if (axes.empty()){
        std::copy_n(input,totalElements, output);
    }
    else if (axes.size() == 1) {
        const std::size_t stride_pre = std::accumulate(inputDims.cbegin(), inputDims.cbegin() + axes[0], 1, std::multiplies<std::size_t>());
        const std::size_t stride_post = std::accumulate(inputDims.crbegin(), inputDims.crbegin() + nb_dims -1 - axes[0], 1, std::multiplies<std::size_t>());

        const std::size_t dim_i = inputDims[axes[0]];
        for (std::size_t pre = 0; pre < stride_pre; ++pre) {
            for (std::size_t post = 0; post < stride_post; ++post) {
                const std::size_t idx_i = pre * dim_i * stride_post + post;
                const std::size_t idx_o = pre * stride_post + post;
                output[idx_o]  = castFromFloat<O>(stableMean(input + idx_i, dim_i, stride_post));
            }
        }
    } else {
        std::size_t outputElements = totalElements;

        auto stride_post = std::unique_ptr<std::size_t[]>(new std::size_t[nb_dims]);
        stride_post[nb_dims - 1] = 1;
        for (std::size_t i = nb_dims-2; i != static_cast<std::size_t>(-1); --i) {
            stride_post[i] = stride_post[i+1]*inputDims[i+1];
        }
        auto stride_pre = std::unique_ptr<std::size_t[]>(new std::size_t[nb_dims]);
        stride_pre[0] = 1;
        for (std::size_t i = 1; i < nb_dims; ++i) {
            stride_pre[i] = stride_pre[i-1]*inputDims[i-1];
        }

        // Type should be the return type of stableMean<I>(), which is always floating point
        const decltype(stableMean<I>(input, 0, 0))* inputAccumulation = nullptr;
        decltype(stableMean<I>(input, 0, 0))* outputAccumulation = nullptr;

        for (const auto& axisInt : axes) {
            const std::size_t a = static_cast<std::size_t>(axisInt);
            outputElements /= inputDims[a];
            outputAccumulation = new I[outputElements];
            const std::size_t dim_i = inputDims[a];
            for (std::size_t pre = 0; pre < stride_pre[a]; ++pre) {
                for (std::size_t post = 0; post < stride_post[a]; ++post) {
                    const std::size_t idx_i = pre * dim_i * stride_post[a] + post;
                    const std::size_t idx_o = pre * stride_post[a] + post;
                    if (inputAccumulation == nullptr) {
                        outputAccumulation[idx_o] = stableMean<I>(input + idx_i, dim_i, stride_post[a]);
                    }
                    else {
                        outputAccumulation[idx_o] = stableMean<I>(inputAccumulation + idx_i, dim_i, stride_post[a]);
                    }
                }
            }
            std::for_each(stride_pre.get()+a+1, stride_pre.get()+nb_dims, [dim_i] (std::size_t& val) { val /= dim_i; });
            if (inputAccumulation != nullptr) {
                delete[] inputAccumulation;
            }
            inputAccumulation = outputAccumulation;
        }

        std::transform(inputAccumulation, inputAccumulation + outputElements, output,
            [](auto value) { return castFromFloat<O>(value); });
        if (outputAccumulation) {
            delete[] outputAccumulation;
        }
    }
}

// Kernels registration to implementation entry point
REGISTRAR(ReduceMeanImpl_cpu,
    {DataType::Float32},
    {ProdConso::inPlaceModel, Aidge::ReduceMeanImpl_cpu_forward_kernel<float, float>, nullptr});
REGISTRAR(ReduceMeanImpl_cpu,
    {DataType::Float64},
    {ProdConso::inPlaceModel, Aidge::ReduceMeanImpl_cpu_forward_kernel<double, double>, nullptr});
REGISTRAR(ReduceMeanImpl_cpu,
    {DataType::Int32},
    {ProdConso::inPlaceModel, Aidge::ReduceMeanImpl_cpu_forward_kernel<int32_t, int32_t>, nullptr});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_KERNELS_H_ */
