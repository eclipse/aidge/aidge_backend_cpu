/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_MATMULIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_MATMULIMPL_KERNELS_H_

#include "aidge/backend/cpu/operator/MatMulImpl.hpp"

namespace Aidge {

template <class I, class O>
void MatMulImpl_cpu_forward_kernel(const std::size_t n, const std::size_t k, const std::size_t m,
                                    const void* input1_, const void* input2_, void* __restrict output_) {
    // FIXME: missing MatMul parameters as arguments
    const I* input1 = static_cast<const I*>(input1_);
    const I* input2 = static_cast<const I*>(input2_);
    O* __restrict output = static_cast<O* __restrict>(output_);

    std::memset(output, O(0), n * m * sizeof(O));

    for (std::size_t i = 0; i < n; ++i) {
        for (std::size_t l = 0; l < k; ++l) {
            for (std::size_t j = 0; j < m; ++j) {
                output[i*m + j] += static_cast<O>(input1[i*k + l] * input2[l*m + j]);
            }
        }
    }
}

// Kernels registration to implementation entry point
REGISTRAR(MatMulImpl_cpu,
    {DataType::Float32},
    {ProdConso::defaultModel, Aidge::MatMulImpl_cpu_forward_kernel<float, float>, nullptr});
REGISTRAR(MatMulImpl_cpu,
    {DataType::Float64},
    {ProdConso::defaultModel, Aidge::MatMulImpl_cpu_forward_kernel<double, double>, nullptr});
REGISTRAR(MatMulImpl_cpu,
    {DataType::Int32},
    {ProdConso::defaultModel, Aidge::MatMulImpl_cpu_forward_kernel<int32_t, int32_t>, nullptr});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_MATMULIMPL_KERNELS_H_ */
