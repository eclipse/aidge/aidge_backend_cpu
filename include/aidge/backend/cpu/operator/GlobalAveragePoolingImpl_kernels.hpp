/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_GLOBALAVERAGEPOOLINGIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_GLOBALAVERAGEPOOLINGIMPL_KERNELS_H_

#include <cstddef>
#include <functional>  // std::multiplies
#include <numeric>     // std::accumulate
#include <vector>

#include "aidge/backend/cpu/operator/GlobalAveragePoolingImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"


namespace Aidge {

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, T>::type
stableMean(const T* vec, size_t size) {
  T mean = 0;
  for (size_t i = 0; i < size; ++i) {
    mean = std::fma<T>(vec[i] - mean, 1.0f / (i + 1), mean);
  }
  return mean;
}

// Specialization for integers: perform the mean computation in float
template <typename T>
typename std::enable_if<!std::is_floating_point<T>::value, T>::type
stableMean(const T* vec, size_t size) {
  double mean = 0;
  for (size_t i = 0; i < size; ++i) {
    mean = std::fma<double>(vec[i] - mean, 1.0f / (i + 1), mean);
  }
  return mean;
}

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, T>::type
castFromFloat(T value) {
  return value;
}

template <typename T>
typename std::enable_if<!std::is_floating_point<T>::value, T>::type
castFromFloat(double value) {
  return static_cast<T>(std::nearbyint(value));
}

template <class I, class O>
void GlobalAveragePoolingImpl_cpu_forward_kernel(
    const std::vector<DimSize_t> &dims, const void *input_, void *output_) {
  // error checking
    AIDGE_ASSERT(dims.size() >= 3,"GlobalAveragePool needs at least a 3 dimensions "
                 "input, number of input dim : {}",
                 dims.size());

  // computation
  const I *input = static_cast<const I *>(input_);
  O *output = static_cast<O *>(output_);

  DimSize_t nb_elems = std::accumulate(dims.begin(), dims.end(), std::size_t(1),
                                       std::multiplies<std::size_t>());

  const DimSize_t in_batch_nb_elems{nb_elems / dims[0]};
  const DimSize_t in_channel_nb_elems{in_batch_nb_elems / dims[1]};
  const DimSize_t out_batch_nb_elems{dims[1]};
  // parse channel by channel and fill each output with the average of the
  // values in the channel
  for (DimSize_t batch = 0; batch < dims[0]; ++batch) {
    for (DimSize_t channel = 0; channel < dims[1]; ++channel) {
      const I *filter_start = std::next(
          input, (batch * in_batch_nb_elems) + (channel * in_channel_nb_elems));
      output[batch * out_batch_nb_elems + channel] = castFromFloat<O>(stableMean<I>(filter_start, in_channel_nb_elems));
    }
  }
}

// Kernels registration to implementation entry point
REGISTRAR(GlobalAveragePoolingImpl_cpu,
    {DataType::Float32},
    {ProdConso::defaultModel, Aidge::GlobalAveragePoolingImpl_cpu_forward_kernel<float, float>, nullptr});
REGISTRAR(GlobalAveragePoolingImpl_cpu,
    {DataType::Float64},
    {ProdConso::defaultModel, Aidge::GlobalAveragePoolingImpl_cpu_forward_kernel<double, double>, nullptr});
REGISTRAR(GlobalAveragePoolingImpl_cpu,
    {DataType::Int32},
    {ProdConso::defaultModel, Aidge::GlobalAveragePoolingImpl_cpu_forward_kernel<int32_t, int32_t>, nullptr});
} // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_GLOBALAVERAGEPOOLINGIMPL_KERNELS_H_ */
