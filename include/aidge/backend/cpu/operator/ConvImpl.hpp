/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_CONVIMPL_H_
#define AIDGE_CPU_OPERATOR_CONVIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// Operator implementation entry point for the backend
using Conv1D_Op = Conv_Op<1>;
using ConvImpl1D_cpu = OperatorImpl_cpu<Conv_Op<1>,
    void(const std::array<DimSize_t, 1>&,
        const std::array<DimSize_t, 1>&,
        const std::array<DimSize_t, 1>&,
        const std::array<DimSize_t, 3> &,
        DimSize_t,
        const void *,
        const void *,
        const void *,
        void *)>;

using Conv2D_Op = Conv_Op<2>;
using ConvImpl2D_cpu = OperatorImpl_cpu<Conv_Op<2>,
    void(const std::array<DimSize_t, 2>&,
        const std::array<DimSize_t, 2>&,
        const std::array<DimSize_t, 2>&,
        const std::array<DimSize_t, 4> &,
        DimSize_t,
        const void *,
        const void *,
        const void *,
        void *)>;

// Implementation entry point registration to Operator
REGISTRAR(Conv1D_Op, "cpu", Aidge::ConvImpl1D_cpu::create);
REGISTRAR(Conv2D_Op, "cpu", Aidge::ConvImpl2D_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_CONVIMPL_H_ */
