/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_ROUNDIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_ROUNDIMPL_KERNELS_H_

#include <cmath>   //std::round 
#include <cstddef>  // std::size_t

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/RoundImpl.hpp"

namespace Aidge {
template <class I, class O>
void RoundImpl_cpu_forward_kernel(const std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        //std::round would not work since it doesn't follow the halves rules (See ONNX Round)
        output[i] = static_cast<O>(std::nearbyint(static_cast<float>(input[i])));
    }
}


REGISTRAR(RoundImpl_cpu,
    {DataType::Float32},
    {ProdConso::inPlaceModel, Aidge::RoundImpl_cpu_forward_kernel<float, float>,nullptr});
REGISTRAR(RoundImpl_cpu,
    {DataType::Float64},
    {ProdConso::inPlaceModel, Aidge::RoundImpl_cpu_forward_kernel<double, double>,nullptr});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_ROUNDIMPL_KERNELS_H_ */
