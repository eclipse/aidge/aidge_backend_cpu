/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_MULIMPL_H_
#define AIDGE_CPU_OPERATOR_MULIMPL_H_

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <memory>
#include <vector>

namespace Aidge {
// Operator implementation entry point for the backend
using MulImpl_cpu = OperatorImpl_cpu<Mul_Op,
    void(std::vector<std::size_t>,
        std::vector<std::size_t>,
        const std::vector<std::size_t>&,
        const void*,
        const void*,
        void*),
    void(const std::size_t,
        const std::size_t,
        const std::size_t,
        const std::vector<std::size_t>,
        const std::vector<std::size_t>,
        const std::vector<std::size_t>,
        const void*,
        const void*,
        const void*,
        void*,
        void*)>;

// Implementation entry point registration to Operator
REGISTRAR(Mul_Op, "cpu", Aidge::MulImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_MULIMPL_H_ */
