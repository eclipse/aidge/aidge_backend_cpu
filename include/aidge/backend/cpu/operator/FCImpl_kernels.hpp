/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_FCIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_FCIMPL_KERNELS_H_

#include <algorithm>

#include "aidge/backend/cpu/operator/FCImpl.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {
// template <class I, class W, class B, class O>
// void FCImpl_cpu_forward_kernel(const FC_Op::Attrs& attrs, const std::array<DimSize_t, 4>& dims,
//                                    const void* input_, const void* weights_, const void* biases_, void* output_) {
//     // FIXME: missing FC attributes as arguments
//     const I* input = static_cast<const I*>(input_);
//     const W* weights = static_cast<const W*>(weights_);
//     const B* biases = static_cast<const B*>(biases_);
//     O* output = static_cast<O*>(output_);

//     for (std::size_t outIdx = 0; outIdx < outputFeatureSize; ++outIdx) {
//         std::size_t oIndex = outIdx * dims[3];
//         const B bias = std::get<0>(attrs) ? B(0) : biases[outIdx];
//         for (std::size_t batch = 0; batch < dims[3]; ++batch) {
//             output[oIndex + batch] = bias;
//         }
//     }

//     for (std::size_t ix = 0; ix < dims[0]; ++ix) {
//         for (std::size_t iy = 0; iy < dims[1]; ++iy) {
//             for (std::size_t inCh = 0; inCh < dims[2]; ++inCh) {
//                 const std::size_t iIndex = dims[3] * (inCh + dims[2] * (iy + dims[1] * ix));
//                 for (std::size_t outCh = 0; outCh < outputFeatureSize; ++outCh) {
//                     const std::size_t oIndex = dims[3] * outCh;
//                     const std::size_t wIndex = (inCh + dims[2] * (iy + dims[1] * ix)) * outputFeatureSize +
//                                           outCh;  // (iIndex*outputFeatureSize + oIndex)/dims[3];
//                     for (std::size_t batch = 0; batch < dims[3]; ++batch) {
//                         output[oIndex + batch] += weights[wIndex] * input[iIndex + batch];
//                     }
//                 }
//             }
//         }
//     }
// }

// template <class I, class W, class B, class O>
// void FCImpl_cpu_forward_kernel(const FC_Op::Attrs& attrs, const std::array<DimSize_t, 2>& dims,
//                                    const void* input_, const void* weights_, const void* biases_, void* output_) {
//     // FIXME: missing FC attributes as arguments
//     const I* input = static_cast<const I*>(input_);
//     const W* weights = static_cast<const W*>(weights_);
//     const B* biases = static_cast<const B*>(biases_);
//     O* output = static_cast<O*>(output_);

//     // let's have I.dims() = [N, C, H, W] instead of [H, W, C, N]

//     for (std::size_t outIdx = 0; outIdx < outputFeatureSize; ++outIdx) {
//         std::size_t oIndex = outIdx * dims[0];
//         const B bias = std::get<0>(attrs) ? B(0) : biases[outIdx];
//         for (std::size_t batch = 0; batch < dims[0]; ++batch) {
//             output[oIndex + batch] = bias;
//         }
//     }

//     for (std::size_t batch = 0; batch < dims[0]; ++batch) {
//         const std::size_t oIndex = dims[1] * batch;
//         for (std::size_t i = 0; i < dims[1]; ++i) {
//             for (std::size_t outCh = 0; outCh < outputFeatureSize; ++outCh) {
//                 std::size_t wIndex = i * outputFeatureSize + outCh;  // (iIndex*outputFeatureSize + oIndex)/dims[3];
//                 output[oIndex + outCh] += weights[wIndex] * input[i + batch];
//             }
//         }
//     }
// }

template <class I, class W, class B, class O>
void FCImpl_cpu_forward_kernel(const DimSize_t batchSize,
                            const DimSize_t inputFeatureSize,
                            const DimSize_t outputFeatureSize,
                            const void* input_,
                            const void* weights_,
                            const void* biases_,
                            void* output_) {
    // FIXME: missing FC attributes as arguments
    const I* input = static_cast<const I*>(input_);
    const W* weights = static_cast<const W*>(weights_);
    const B* biases = static_cast<const B*>(biases_);
    O* output = static_cast<O*>(output_);

    if (biases == nullptr) {
        std::fill(output, output+(batchSize*outputFeatureSize), B(0));
    }
    else {
        for (std::size_t batch = 0; batch < batchSize; ++batch) {
            std::copy(biases, biases+outputFeatureSize, output+(batch*outputFeatureSize));
        }
    }

    for (std::size_t batch = 0; batch < batchSize; ++batch) {
        for (std::size_t out = 0; out < outputFeatureSize; ++out) {
            output[out + batch*outputFeatureSize] = std::inner_product(input + batch*inputFeatureSize,
                                                        input + (batch + 1)*inputFeatureSize,
                                                        weights + out*inputFeatureSize,
                                                        output[out + batch*outputFeatureSize]);
        }
    }
}

template <class I, class O, class W, class B>
void FCImpl_cpu_backward_kernel(const DimSize_t batchSize,
                                const DimSize_t inputFeatureSize,
                                const DimSize_t outputFeatureSize,
                                const void* input_,
                                const void* originalInput_,
                                const void* weight_,
                                void* output_,
                                void* weightGrad_,
                                void* biasesGrad_)
{
    // FIXME: missing FC attributes as arguments
    const I* input  = static_cast<const I*>(input_);
    const I* originalInput  = static_cast<const I*>(originalInput_);
    const W* weight = static_cast<const W*>(weight_);
    O* output       = static_cast<O*>(output_);
    W* weightGrad   = static_cast<W*>(weightGrad_);
    B* biasesGrad   = static_cast<B*>(biasesGrad_);


    // bias grad
    if (biasesGrad == nullptr) { // no bias
        std::fill(biasesGrad, biasesGrad + outputFeatureSize, B(0));
    } else {
        for (std::size_t o = 0; o < outputFeatureSize; ++o) { // nb outputs
            B sum{0};
            for (std::size_t b = 0; b < batchSize; ++b) {
                sum += input[b*outputFeatureSize + o];
            }
            biasesGrad[o] = sum;
        }
    }

    // weight grad
    for (std::size_t o = 0; o < outputFeatureSize; ++o) {
        for (std::size_t c = 0; c < inputFeatureSize; ++c) {
            W sum{0};
            for (std::size_t b = 0; b < batchSize; ++b) {
                sum += originalInput[b*inputFeatureSize + c]*input[b*outputFeatureSize + o];
            }
            weightGrad[o*inputFeatureSize + c] = sum;
        }
    }

    // input grad
    for (std::size_t b = 0; b < batchSize; ++b) {
        for (std::size_t c = 0; c < inputFeatureSize; ++c) {
            O sum{0};
            for (std::size_t o = 0; o < outputFeatureSize; ++o) {
                sum += weight[o*inputFeatureSize + c] * input[b*outputFeatureSize + o];
            }
            output[b*inputFeatureSize + c] = sum;
        }
    }
}

// Kernels registration to implementation entry point
REGISTRAR(FCImpl_cpu,
    {ImplSpec::IOSpec{DataType::Any}, ImplSpec::IOSpec{DataType::Float32}},
    {ProdConso::defaultModel, Aidge::FCImpl_cpu_forward_kernel<float, float, float, float>, Aidge::FCImpl_cpu_backward_kernel<float, float, float, float>});
REGISTRAR(FCImpl_cpu,
    {ImplSpec::IOSpec{DataType::Any}, ImplSpec::IOSpec{DataType::Float64}},
    {ProdConso::defaultModel, Aidge::FCImpl_cpu_forward_kernel<double, double, double, double>, Aidge::FCImpl_cpu_backward_kernel<double, double, double, double>});
REGISTRAR(FCImpl_cpu,
    {ImplSpec::IOSpec{DataType::Any}, ImplSpec::IOSpec{DataType::Int32}},
    {ProdConso::defaultModel, Aidge::FCImpl_cpu_forward_kernel<int32_t, int32_t, int32_t, int32_t>, Aidge::FCImpl_cpu_backward_kernel<int32_t, int32_t, int32_t, int32_t>});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_FCIMPL_KERNELS_H_ */
