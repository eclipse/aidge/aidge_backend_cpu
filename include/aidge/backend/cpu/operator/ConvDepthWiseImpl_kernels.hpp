/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_KERNELS_H_

#include <algorithm>
#include <array>
#include <cmath>
#include <cstddef>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/ConvDepthWiseImpl.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Forward kernel for 1D ConvDepthWiseolution on CPU backend.
 * @tparam I Input data type.
 * @tparam W Weight data type.
 * @tparam B Bias data type.
 * @tparam O Output data type.
 * @param params tuple of Attributes from the Operator
 * @param inputDims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param weights_ const weight Tensor.
 * @param biases_ const Biais Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class W, class B, class O>
void ConvDepthWiseImpl1D_cpu_forward_kernel(const std::array<DimSize_t, 1>& strideDims,
                            const std::array<DimSize_t, 1>& dilationDims,
                            const std::array<DimSize_t, 1>& kernelDims,
                            const std::array<DimSize_t, 3>& inputDims,
                            const void *input_,
                            const void *weights_,
                            const void *biases_,
                            void *output_) {
    // FIXME: missing convolution attributes as arguments
    const I *input = static_cast<const I *>(input_);
    const W *weights = static_cast<const W *>(weights_);
    const B *biases = static_cast<const B *>(biases_);
    O *output = static_cast<O *>(output_);


    // output H size
    const DimSize_t dilated_kernel_x = dilationDims[0]*(kernelDims[0] - 1) + 1;
    const std::size_t oxSize =
            static_cast<std::size_t>(std::floor(static_cast<float>(inputDims[2] - dilated_kernel_x + strideDims[0]) /
                                static_cast<float>(strideDims[0])));


    // TODO: kernel computation
    // output (batch, outCh, Xout, Yout)
    // input  (batch, ch, Xin, Yin)
    // weight (outCh, ch, kernelX, kernelY)
    // does not take Dilation attribute into account
    using signedsize = std::make_signed<std::size_t>::type;
    for (std::size_t batch = 0; batch < inputDims[0]; ++batch) {
        for (std::size_t ch = 0; ch < inputDims[1]; ++ch) {
            const std::size_t oIndex = (ch + batch*inputDims[1]) * oxSize;
            B biasVal = (biases != nullptr) ? biases[ch] : B(0);
            std::fill(output + oIndex, output+(oIndex+oxSize), biasVal);
            const std::size_t iIndex = (ch + batch*inputDims[1]) * inputDims[2];
            const std::size_t wIndex = ch * kernelDims[0];
            for (std::size_t ox = 0; ox < oxSize; ++ox) {
                // const signedsize difx = static_cast<signedsize>(- ox * strideDims[0]);
                // const std::size_t sxMin = static_cast<std::size_t>(std::max(difx, signedsize(0)));
                // const std::size_t sxMax = (static_cast<signedsize>(inputDims[2]) + difx) < 0 ? 0 : ((inputDims[2] + difx) > kernelDims[0] ? kernelDims[0] : inputDims[2] + difx);
                const std::size_t sxMin = 0;
                const std::size_t sxMax = dilated_kernel_x;
                const std::size_t oIndexFull = oIndex + ox;
                const signedsize ix = static_cast<signedsize>(ox * strideDims[0]);

                for (std::size_t sx = sxMin; sx*dilationDims[0] < sxMax; ++sx) {
                    output[oIndexFull] += weights[wIndex + sx] *
                                            input[iIndex + static_cast<std::size_t>(ix+static_cast<signedsize>(sx*dilationDims[0]))];
                }
            }
        }
    }
}

// Kernels registration to implementation entry point
REGISTRAR(ConvDepthWiseImpl1D_cpu,
    {{DataType::Any, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::ConvDepthWiseImpl1D_cpu_forward_kernel<float, float, float, float>, nullptr});
REGISTRAR(ConvDepthWiseImpl1D_cpu,
    {{DataType::Any, DataFormat::NCHW}, {DataType::Int32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::ConvDepthWiseImpl1D_cpu_forward_kernel<std::int32_t, std::int32_t, std::int32_t, std::int32_t>, nullptr});
REGISTRAR(ConvDepthWiseImpl1D_cpu,
    {{DataType::Any, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::ConvDepthWiseImpl1D_cpu_forward_kernel<double, double, double, double>, nullptr});


/**
 * @brief Forward kernel for 2D ConvDepthWiseolution on CPU backend.
 * @tparam I Input data type.
 * @tparam W Weight data type.
 * @tparam B Bias data type.
 * @tparam O Output data type.
 * @param params tuple of Attributes from the Operator
 * @param inputDims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param weights_ const weight Tensor.
 * @param biases_ const Biais Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class W, class B, class O>
void ConvDepthWiseImpl2D_cpu_forward_kernel(const std::array<DimSize_t, 2>& strideDims,
                            const std::array<DimSize_t, 2>& dilationDims,
                            const std::array<DimSize_t, 2>& kernelDims,
                            const std::array<DimSize_t, 4>& inputDims,
                            const void *input_,
                            const void *weights_,
                            const void *biases_,
                            void *output_)
{
    // FIXME: missing convolution attributes as arguments
    const I *input = static_cast<const I *>(input_);
    const W *weights = static_cast<const W *>(weights_);
    const B *biases = static_cast<const B *>(biases_);
    O *output = static_cast<O *>(output_);


    // output H size
    const DimSize_t dilated_kernel_x = dilationDims[0]*(kernelDims[0] - 1) + 1;
    const std::size_t oxSize =
            static_cast<std::size_t>(std::floor(static_cast<float>(inputDims[2] - dilated_kernel_x + strideDims[0]) /
                                static_cast<float>(strideDims[0])));

    // output W size
    const DimSize_t dilated_kernel_y = dilationDims[1]*(kernelDims[1] - 1) + 1;
    const std::size_t oySize =
            static_cast<std::size_t>(std::floor(static_cast<float>(inputDims[3] - dilated_kernel_y + strideDims[1]) /
                                static_cast<float>(strideDims[1])));

    // TODO: kernel computation
    // output (batch, outCh, Xout, Yout)
    // input  (batch, ch, Xin, Yin)
    // weight (outCh, ch, kernelX, kernelY)
    // does not take Dilation attribute into account
    const std::size_t outChannels_s =  oxSize * oySize;

    if (dilated_kernel_x ==3 && dilated_kernel_y == 3) {
        for (std::size_t batch = 0; batch < inputDims[0]; ++batch) {
            for (std::size_t ch = 0; ch < inputDims[1]; ++ch) {

                B biasVal = (biases != nullptr) ? biases[ch] : B(0);

                std::size_t iIndex = (ch + batch*inputDims[1]) * inputDims[2] * inputDims[3];
                const std::size_t wIndex = ch * 9;

                if (strideDims[0] == 1 && strideDims[1]==1) {
                    for (std::size_t ox = 0, oIndex = 0; ox < oxSize; ++ox, oIndex+=oySize, iIndex-=inputDims[3]) {
                        for (std::size_t oy = 0; oy < oySize; ++oy) {
                            output[oIndex + oy] = biasVal + weights[wIndex+0]*input[iIndex+oy]+weights[wIndex+1]*input[iIndex+oy+1]+weights[wIndex+2]*input[iIndex+oy+2];
                        }
                        iIndex+=inputDims[3];
                        for (std::size_t oy = 0; oy < oySize; ++oy) {
                            output[oIndex + oy] += weights[wIndex+3]*input[iIndex+oy]+weights[wIndex+4]*input[iIndex+oy+1]+weights[wIndex+5]*input[iIndex+oy+2];
                        }
                        iIndex+=inputDims[3];
                        for (std::size_t oy = 0; oy < oySize; ++oy) {
                            output[oIndex + oy] += weights[wIndex+6]*input[iIndex+oy]+weights[wIndex+7]*input[iIndex+oy+1]+weights[wIndex+8]*input[iIndex+oy+2];
                        }
                    }
                } else {
                    for (std::size_t ox = 0, oIndex = 0; ox < oxSize; ++ox, oIndex+=oySize, iIndex+=(strideDims[0]-2)*inputDims[3]) {
                        for (std::size_t oy = 0; oy < oySize; ++oy) {
                            output[oIndex + oy] = biasVal + weights[wIndex+0]*input[iIndex+oy*strideDims[1]]+weights[wIndex+1]*input[iIndex+oy*strideDims[1]+1]+weights[wIndex+2]*input[iIndex+oy*strideDims[1]+2];
                        }
                        iIndex+=inputDims[3];
                        for (std::size_t oy = 0; oy < oySize; ++oy) {
                            output[oIndex + oy] += weights[wIndex+3]*input[iIndex+oy*strideDims[1]]+weights[wIndex+4]*input[iIndex+oy*strideDims[1]+1]+weights[wIndex+5]*input[iIndex+oy*strideDims[1]+2];
                        }
                        iIndex+=inputDims[3];
                        for (std::size_t oy = 0; oy < oySize; ++oy) {
                            output[oIndex + oy] += weights[wIndex+6]*input[iIndex+oy*strideDims[1]]+weights[wIndex+7]*input[iIndex+oy*strideDims[1]+1]+weights[wIndex+8]*input[iIndex+oy*strideDims[1]+2];
                        }
                    }
                }
                output += outChannels_s;
            }
        }
    } else if (dilated_kernel_x == 1 && dilated_kernel_y == 1) {
        for (std::size_t batch = 0; batch < inputDims[0]; ++batch) {
            for (std::size_t ch = 0; ch < inputDims[1]; ++ch) {

                B biasVal = (biases != nullptr) ? biases[ch] : B(0);

                std::size_t iIndex = (ch + batch*inputDims[1]) * inputDims[2] * inputDims[3];
                const std::size_t wIndex = ch;

                if (strideDims[0] == 1 && strideDims[1] == 1) {
                    for (std::size_t i = iIndex; i < iIndex + oxSize*oySize; ++i) {
                        output[i] = biasVal + weights[wIndex] * input[i];
                    }
                } else  {
                    std::size_t oIndex =  (ch + batch*inputDims[1]) * oxSize * oySize;
                    for (std::size_t ox = 0; ox < oxSize; ++ox, oIndex+=oySize, iIndex+=strideDims[0]*inputDims[3]) {
                        for (std::size_t oy = 0, iy = 0; oy < oySize; ++oy, iy+=strideDims[1]) {
                            output[oIndex + oy] = biasVal + weights[wIndex]*input[iIndex+iy];
                        }
                    }
                }
            }
        }
    } else {
        for (std::size_t batch = 0; batch < inputDims[0]; ++batch) {
            for (std::size_t ch = 0; ch < inputDims[1]; ++ch) {

                B biasVal = (biases != nullptr) ? biases[ch] : B(0);
                std::fill(output, output+outChannels_s, biasVal);

                const std::size_t iIndex = (ch + batch*inputDims[1]) * inputDims[2] * inputDims[3];
                const std::size_t wIndex = ch * kernelDims[0] * kernelDims[1];

                for (std::size_t ox = 0; ox < oxSize; ++ox) {
                    for (std::size_t oy = 0; oy < oySize; ++oy) {

                        const std::size_t oIndexFull = ox*oySize + oy;
                        const std::size_t ix = ox * strideDims[0];
                        const std::size_t iy = oy * strideDims[1];

                        for (std::size_t kx = 0; kx*dilationDims[0] < dilated_kernel_x; ++kx) {
                            for (std::size_t ky = 0; ky*dilationDims[1] < dilated_kernel_y; ++ky) {
                                output[oIndexFull] += weights[wIndex + kx*kernelDims[1] + ky] *
                                                        input[iIndex + (ix + kx*dilationDims[0])*inputDims[3] + (iy + ky*dilationDims[1])];
                            }
                        }
                    }
                }
                output += outChannels_s;
            }
        }
    }
}


// Kernels registration to implementation entry point
REGISTRAR(ConvDepthWiseImpl2D_cpu,
    {{DataType::Any, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::ConvDepthWiseImpl2D_cpu_forward_kernel<float, float, float, float>, nullptr});
REGISTRAR(ConvDepthWiseImpl2D_cpu,
    {{DataType::Any, DataFormat::NCHW}, {DataType::Int32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::ConvDepthWiseImpl2D_cpu_forward_kernel<std::int32_t, std::int32_t, std::int32_t, std::int32_t>, nullptr});
REGISTRAR(ConvDepthWiseImpl2D_cpu,
    {{DataType::Any, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::ConvDepthWiseImpl2D_cpu_forward_kernel<double, double, double, double>, nullptr});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_KERNELS_H_ */
