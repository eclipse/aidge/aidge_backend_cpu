/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_REDUCESUMIMPL_H_
#define AIDGE_CPU_OPERATOR_REDUCESUMIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/ReduceSum.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// Operator implementation entry point for the backend
using ReduceSumImpl_cpu = OperatorImpl_cpu<ReduceSum_Op,
    void(const std::vector<std::int32_t>&,
                            DimSize_t,
                            const std::vector<DimSize_t>&,
                            const void *,
                            void *)>;

// Implementation entry point registration to Operator
REGISTRAR(ReduceSum_Op, "cpu", Aidge::ReduceSumImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_REDUCESUMIMPL_H_ */
