
/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_CLIPIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_CLIPIMPL_KERNELS_H_

#include "aidge/utils/Registrar.hpp"
#include "aidge/backend/cpu/operator/ClipImpl.hpp"

namespace Aidge {
template <class I, class O>
void ClipImpl_cpu_forward_kernel(
        float min_,
        float max_,
        const void* input_,
        const std::size_t length,
        void* output_) 
{
    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    for (std::size_t i = 0; i < length; ++i) {
        output[i] = std::min(std::max(static_cast<float>(input[i]), min_), max_);
    }
}

template <class I, class GI, class GO>
void ClipImpl_cpu_backward_kernel(
        float min_,
        float max_,
        const std::size_t length,
        const void* input_, 
        const void* grad_output_,
		void* grad_input_)           
{
    const I* input = static_cast<const I*>(input_);
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);

    for (std::size_t i = 0; i < length; ++i) {
        grad_input[i] = ((input[i] > min_) && (input[i] < max_)) ? grad_output[i] : 0;
    }
}

REGISTRAR(ClipImpl_cpu,
{DataType::Float32},
{ProdConso::inPlaceModel,
Aidge::ClipImpl_cpu_forward_kernel<float,float>,
Aidge::ClipImpl_cpu_backward_kernel<float,float,float>});
REGISTRAR(ClipImpl_cpu,
{DataType::Float64},
{ProdConso::inPlaceModel,
Aidge::ClipImpl_cpu_forward_kernel<double,double>,
Aidge::ClipImpl_cpu_backward_kernel<double,double,double>});
REGISTRAR(ClipImpl_cpu,
{DataType::Int32},
{ProdConso::inPlaceModel,
Aidge::ClipImpl_cpu_forward_kernel<std::int32_t,std::int32_t>,
Aidge::ClipImpl_cpu_backward_kernel<std::int32_t,std::int32_t,std::int32_t>});
REGISTRAR(ClipImpl_cpu,
{DataType::Int64},
{ProdConso::inPlaceModel,
Aidge::ClipImpl_cpu_forward_kernel<std::int64_t,std::int64_t>,
Aidge::ClipImpl_cpu_backward_kernel<std::int64_t,std::int64_t,std::int64_t>});

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_CLIPIMPL_KERNELS_H_ */
