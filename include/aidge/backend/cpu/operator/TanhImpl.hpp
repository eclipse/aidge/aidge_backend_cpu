/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_TANHIMPL_H_
#define AIDGE_CPU_OPERATOR_TANHIMPL_H_

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Tanh.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <memory>
#include <vector>

namespace Aidge {
// Operator implementation entry point for the backend
using TanhImpl_cpu = OperatorImpl_cpu<Tanh_Op,
    void(const std::size_t, const void*, void*),
    void(const std::size_t, const void*, const void*, void*)>;

// Implementation entry point registration to Operator
REGISTRAR(Tanh_Op, "cpu", Aidge::TanhImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_TANHIMPL_H_ */
