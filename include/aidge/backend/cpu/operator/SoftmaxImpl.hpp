/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SOFTMAXIMPL_H_
#define AIDGE_CPU_OPERATOR_SOFTMAXIMPL_H_

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Softmax.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <memory>
#include <vector>

namespace Aidge {
// Operator implementation entry point for the backend
using SoftmaxImpl_cpu = OperatorImpl_cpu<Softmax_Op,
    void(std::size_t, const std::vector<DimSize_t>&, const void*, void*)>;

// Implementation entry point registration to Operator
REGISTRAR(Softmax_Op, "cpu", Aidge::SoftmaxImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SOFTMAXIMPL_H_ */
