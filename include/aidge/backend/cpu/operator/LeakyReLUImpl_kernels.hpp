/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_LEAKYRELUIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_LEAKYRELUIMPL_KERNELS_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/LeakyReLUImpl.hpp"

namespace Aidge {
template <class I, class O>
void LeakyReLUImpl_cpu_forward_kernel(const float negativeSlope_,
                                     std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);
    const I negativeSlope = static_cast<const I>(negativeSlope_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = (input[i] >= 0) ? input[i] : input[i] * negativeSlope;
    }
}

template <class I, class O>
void LeakyReLUImpl_cpu_backward_kernel(const float negativeSlope_,
                                     std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);
    const I negativeSlope = static_cast<const I>(negativeSlope_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = (input[i] > 0) ? input[i] : negativeSlope*input[i];
    }
}

// Kernels registration to implementation entry point
REGISTRAR(LeakyReLUImpl_cpu,
    {DataType::Float32},
    {ProdConso::inPlaceModel, Aidge::LeakyReLUImpl_cpu_forward_kernel<float, float>, Aidge::LeakyReLUImpl_cpu_backward_kernel<float, float>});
REGISTRAR(LeakyReLUImpl_cpu,
    {DataType::Float64},
    {ProdConso::inPlaceModel, Aidge::LeakyReLUImpl_cpu_forward_kernel<double, double>, Aidge::LeakyReLUImpl_cpu_backward_kernel<double, double>});
REGISTRAR(LeakyReLUImpl_cpu,
    {DataType::Int32},
    {ProdConso::inPlaceModel, Aidge::LeakyReLUImpl_cpu_forward_kernel<int32_t, int32_t>, Aidge::LeakyReLUImpl_cpu_backward_kernel<int32_t, int32_t>});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_LEAKYRELUIMPL_KERNELS_H_ */
