/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_FOLDIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_FOLDIMPL_KERNELS_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/FoldImpl.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <cmath>
#include <array>
#include <algorithm>

namespace Aidge {
template <class I, class O>
void FoldImpl2D_cpu_forward_kernel(const std::array<DimSize_t, 2>& outputDims,
                                    const std::array<DimSize_t, 2>& strideDims,
                                    const std::array<DimSize_t, 2>& dilationDims,
                                    const std::array<DimSize_t, 2>& kernelDims,
                                    const std::vector<DimSize_t> &dims,
                                    const void *input_, void *output_)
{
    const I *input = static_cast<const I *>(input_);
    O *output = static_cast<O *>(output_);

    const DimSize_t inHeight = outputDims[0];
    const DimSize_t inWidth = outputDims[1];

    const DimSize_t kernelExtentHeight = dilationDims[0] *
                                            (kernelDims[0] - 1) + 1;
    const DimSize_t outHeight = 1 + static_cast<DimSize_t>(
                    floor(static_cast<float>(inHeight - kernelExtentHeight) /
                            static_cast<float>(strideDims[0])));
    const DimSize_t kernelExtentWidth = dilationDims[1] *
                                            (kernelDims[1] - 1) + 1;
    const DimSize_t outWidth = 1 + static_cast<DimSize_t>(
                    floor(static_cast<float>(inWidth - kernelExtentWidth) /
                            static_cast<float>(strideDims[1])));
    const DimSize_t outChannels = dims[dims.size() - 2];
    const DimSize_t inChannels = outChannels / kernelDims[0] / kernelDims[1];

    std::fill_n(output, dims[0] * outHeight * outWidth * outChannels, O(0));

    for (DimSize_t n = 0; n < dims[0]; ++n) {
        for (DimSize_t outC = 0; outC < outChannels; ++outC) {
            const auto inOffsetW = outC % kernelDims[1];
            const auto inOffsetH = (outC / kernelDims[1]) % kernelDims[0];
            const auto inC = outC / kernelDims[0] / kernelDims[1];

            for (DimSize_t outH = 0; outH < outHeight; ++outH) {
                const auto inH = outH * strideDims[0] + inOffsetH * dilationDims[0];

                for (DimSize_t outW = 0; outW < outWidth; ++outW) {
                    const auto inW = outW * strideDims[1] + inOffsetW * dilationDims[1];

                    output[((n * inChannels + inC) * inHeight + inH) * inWidth + inW] +=
                        input[((n * outChannels + outC) * outHeight + outH) * outWidth + outW];
                }
            }
        }
    }
}

// Kernels registration to implementation entry point
REGISTRAR(FoldImpl2D_cpu,
    {DataType::Float32},
    {ProdConso::defaultModel, Aidge::FoldImpl2D_cpu_forward_kernel<float, float>, nullptr});
REGISTRAR(FoldImpl2D_cpu,
    {DataType::Float64},
    {ProdConso::defaultModel, Aidge::FoldImpl2D_cpu_forward_kernel<double, double>, nullptr});
REGISTRAR(FoldImpl2D_cpu,
    {DataType::Int32},
    {ProdConso::defaultModel, Aidge::FoldImpl2D_cpu_forward_kernel<int32_t, int32_t>, nullptr});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_FOLDIMPL_KERNELS_H_ */
