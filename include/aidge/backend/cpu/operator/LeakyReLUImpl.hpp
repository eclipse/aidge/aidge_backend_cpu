/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_LEAKYRELUIMPL_H_
#define AIDGE_CPU_OPERATOR_LEAKYRELUIMPL_H_

#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/LeakyReLU.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// Operator implementation entry point for the backend
using LeakyReLUImpl_cpu = OperatorImpl_cpu<LeakyReLU_Op,
    void(const float,
        std::size_t,
        const void*,
        void*),
    void(const float,
        std::size_t,
        const void*,
        void*)>;

// Implementation entry point registration to Operator
REGISTRAR(LeakyReLU_Op, "cpu", Aidge::LeakyReLUImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_LEAKYRELUIMPL_H_ */
