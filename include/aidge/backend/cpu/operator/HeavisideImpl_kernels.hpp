/********************************************************************************
 * Copyright (c) 2025 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_HEAVISIDEIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_HEAVISIDEIMPL_KERNELS_H_

#include "aidge/utils/Registrar.hpp"

#include <cstddef> // std::size_t

#include "aidge/backend/cpu/operator/HeavisideImpl.hpp"
#include "aidge/utils/ErrorHandling.hpp"


namespace Aidge {

template <class I, class O>
void HeavisideImplCpuForwardKernel(std::size_t inputLenght,
                                   const void *input_,
                                   void *output_,
                                   const float value) {
    const I *input = static_cast<const I *>(input_);
    O *output = static_cast<O *>(output_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = (input[i] > 0) ? 1 : (input[i] == 0 ? value : 0);
    }
}

// Kernels registration to implementation entry point
REGISTRAR(HeavisideImplCpu,
          {DataType::Float32},
          {ProdConso::inPlaceModel,
           Aidge::HeavisideImplCpuForwardKernel<float, float>,
           nullptr});
} // namespace Aidge

#endif // AIDGE_CPU_OPERATOR_HEAVISIDEIMPL_KERNELS_H__H_
