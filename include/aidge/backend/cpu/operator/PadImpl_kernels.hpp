/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_PADIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_PADIMPL_KERNELS_H_

#include <algorithm>  // std::max, std::min
#include <array>
#include <cstddef>    // std::size_t
#include <cstdint>    // std::int32_t

#include "aidge/backend/cpu/operator/PadImpl.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Forward kernel for 1D Padding on CPU backend.
 * @tparam I Input data type.
 * @tparam O Output data type.
 * @param attrs tuple of Parameters from the Operator
 * @param dims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class O>
void PadImpl1D_cpu_forward_kernel(const std::array<DimSize_t, 2>& beginEndBorders,
                                const PadBorderType borderType,
                                const double borderValue,
                                const std::array<DimSize_t, 3>& dims,
                                const void *input_,
                                void *output_)
{
    const I *input = static_cast<const I *>(input_);
    O *output = static_cast<O *>(output_);

    const std::size_t oxSize = dims[2] + beginEndBorders[0] + beginEndBorders[1];

    for (std::size_t batch = 0; batch < dims[0]; ++batch) {
        for (std::size_t ch = 0; ch < dims[1]; ++ch) {
            const std::size_t iIndex = (ch + batch*dims[1]) * dims[2];
            const std::size_t oIndex = (ch + batch*dims[1]) * oxSize;

            for (unsigned int ox = 0; ox < oxSize; ++ox) {
                const std::size_t oIndexFull = oIndex + ox;

                O outputValue = static_cast<O>(borderValue);

                if (borderType == PadBorderType::Constant) {
                    int ix = static_cast<int>(ox) - static_cast<int>(beginEndBorders[0]);

                    if (ix >= 0  && ix < static_cast<int>(dims[2])) {
                        outputValue = input[iIndex + static_cast<std::size_t>(ix)];
                    }
                }
                else if (borderType == PadBorderType::Edge) {
                    int ix = std::max(0, std::min(static_cast<int>(dims[2]) - 1, static_cast<int>(ox) - static_cast<int>(beginEndBorders[0])));

                    outputValue = input[iIndex + static_cast<std::size_t>(ix)];
                }
                else if (borderType == PadBorderType::Reflect) {
                    int ix = static_cast<int>(ox) - static_cast<int>(beginEndBorders[0]);

                    if (ix < 0)
                        ix = 0 - ix;
                    if (ix >= static_cast<int>(dims[2]))
                        ix = static_cast<int>(dims[2]) - ix;

                    outputValue = input[iIndex + static_cast<std::size_t>(ix)];
                }
                else if (borderType == PadBorderType::Wrap) {
                    int ix = (static_cast<int>(dims[2]) + static_cast<int>(ox) - static_cast<int>(beginEndBorders[0])) % static_cast<int>(dims[2]);

                    outputValue = input[iIndex + static_cast<std::size_t>(ix)];
                }

                output[oIndexFull] = outputValue;
            }
        }
    }
}

// Kernels registration to implementation entry point
REGISTRAR(PadImpl1D_cpu,
    {{DataType::Float32, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {Pad_ProdConso_cpu::defaultModel, Aidge::PadImpl1D_cpu_forward_kernel<cpptype_t<DataType::Float32>, cpptype_t<DataType::Float32>>, nullptr});
REGISTRAR(PadImpl1D_cpu,
    {{DataType::Float64, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {Pad_ProdConso_cpu::defaultModel, Aidge::PadImpl1D_cpu_forward_kernel<cpptype_t<DataType::Float64>, cpptype_t<DataType::Float64>>, nullptr});
REGISTRAR(PadImpl1D_cpu,
    {{DataType::Int32, DataFormat::NCHW}, {DataType::Int32, DataFormat::NCHW}},
    {Pad_ProdConso_cpu::defaultModel, Aidge::PadImpl1D_cpu_forward_kernel<cpptype_t<DataType::Int32>, cpptype_t<DataType::Int32>>, nullptr});


/**
 * @brief Forward kernel for 2D Padding on CPU backend.
 * @tparam I Input data type.
 * @tparam O Output data type.
 * @param attrs tuple of Parameters from the Operator
 * @param dims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class O>
void PadImpl2D_cpu_forward_kernel(const std::array<DimSize_t, 4>& beginEndBorders,
                                const PadBorderType borderType,
                                const double borderValue,
                                const std::array<DimSize_t, 4> &dims,
                                const void *input_,
                                void *output_)
{
    const I *input = static_cast<const I *>(input_);
    O *output = static_cast<O *>(output_);

    const std::size_t oySize = dims[2] + beginEndBorders[0] + beginEndBorders[2];
    const std::size_t oxSize = dims[3] + beginEndBorders[1] + beginEndBorders[3];

    for (std::size_t batch = 0; batch < dims[0]; ++batch) {
        for (std::size_t ch = 0; ch < dims[1]; ++ch) {
            const std::size_t iIndex = (ch + batch*dims[1]) * dims[2] * dims[3];
            const std::size_t oIndex = (ch + batch*dims[1]) * oxSize * oySize;

            for (std::uint32_t oy = 0; oy < oySize; ++oy) {
                for (std::uint32_t ox = 0; ox < oxSize; ++ox) {
                    const std::size_t oIndexFull = oIndex + oy*oxSize + ox;

                    O outputValue = static_cast<O>(borderValue);

                    if (borderType == PadBorderType::Constant) {
                        std::int32_t ix = static_cast<std::int32_t>(ox) - static_cast<std::int32_t>(beginEndBorders[1]);
                        std::int32_t iy = static_cast<std::int32_t>(oy) - static_cast<std::int32_t>(beginEndBorders[0]);

                        if (ix >= 0  && ix < static_cast<std::int32_t>(dims[3]) && iy >= 0  && iy < static_cast<std::int32_t>(dims[2])) {
                            outputValue = input[iIndex + static_cast<std::size_t>(iy)*dims[3] + static_cast<std::size_t>(ix)];
                        }
                    }
                    else if (borderType == PadBorderType::Edge) {
                        std::int32_t ix = std::max(0, std::min(static_cast<std::int32_t>(dims[3]) - 1, static_cast<std::int32_t>(ox) - static_cast<std::int32_t>(beginEndBorders[1])));
                        std::int32_t iy = std::max(0, std::min(static_cast<std::int32_t>(dims[2]) - 1, static_cast<std::int32_t>(oy) - static_cast<std::int32_t>(beginEndBorders[0])));

                        outputValue = input[iIndex + static_cast<std::size_t>(iy)*dims[3] + static_cast<std::size_t>(ix)];
                    }
                    else if (borderType == PadBorderType::Reflect) {
                        std::int32_t ix = static_cast<std::int32_t>(ox) - static_cast<std::int32_t>(beginEndBorders[1]);
                        std::int32_t iy = static_cast<std::int32_t>(oy) - static_cast<std::int32_t>(beginEndBorders[0]);

                        if (ix < 0)
                            ix = 0 - ix;
                        if (iy < 0)
                            iy = 0 - iy;
                        if (ix >= static_cast<std::int32_t>(dims[3]))
                            ix = static_cast<std::int32_t>(dims[3]) - ix;
                        if (iy >= static_cast<std::int32_t>(dims[2]))
                            iy = static_cast<std::int32_t>(dims[2]) - iy;

                        outputValue = input[iIndex + static_cast<std::size_t>(iy)*dims[3] + static_cast<std::size_t>(ix)];
                    }
                    else if (borderType == PadBorderType::Wrap) {
                        std::int32_t ix = (static_cast<std::int32_t>(dims[3]) + static_cast<std::int32_t>(ox) - static_cast<std::int32_t>(beginEndBorders[1])) % static_cast<std::int32_t>(dims[3]);
                        std::int32_t iy = (static_cast<std::int32_t>(dims[2]) + static_cast<std::int32_t>(oy) - static_cast<std::int32_t>(beginEndBorders[0])) % static_cast<std::int32_t>(dims[2]);

                        outputValue = input[iIndex + static_cast<std::size_t>(iy)*dims[3] + static_cast<std::size_t>(ix)];
                    }

                    output[oIndexFull] = outputValue;
                }
            }
        }
    }
}

// Kernels registration to implementation entry point
REGISTRAR(PadImpl2D_cpu,
    {{DataType::Float32, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {Pad_ProdConso_cpu::defaultModel, Aidge::PadImpl2D_cpu_forward_kernel<cpptype_t<DataType::Float32>, cpptype_t<DataType::Float32>>, nullptr});
REGISTRAR(PadImpl2D_cpu,
    {{DataType::Float64, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {Pad_ProdConso_cpu::defaultModel, Aidge::PadImpl2D_cpu_forward_kernel<cpptype_t<DataType::Float64>, cpptype_t<DataType::Float64>>, nullptr});
REGISTRAR(PadImpl2D_cpu,
    {{DataType::Int32, DataFormat::NCHW}, {DataType::Int32, DataFormat::NCHW}},
    {Pad_ProdConso_cpu::defaultModel, Aidge::PadImpl2D_cpu_forward_kernel<cpptype_t<DataType::Int32>, cpptype_t<DataType::Int32>>, nullptr});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_PADIMPL_KERNELS_H_ */
