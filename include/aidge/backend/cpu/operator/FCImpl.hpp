/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_FCIMPL_H_
#define AIDGE_CPU_OPERATOR_FCIMPL_H_

#include <array>
#include <memory>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// Operator implementation entry point for the backend
using FCImpl_cpu = OperatorImpl_cpu<FC_Op,
    void(const DimSize_t,
        const DimSize_t,
        const DimSize_t,
        const void *,
        const void *,
        const void *,
        void *),
    void(const DimSize_t,
        const DimSize_t,
        const DimSize_t,
        const void *,
        const void *,
        const void *,
        void *,
        void *,
        void *)>;

// Implementation entry point registration to Operator
REGISTRAR(FC_Op, "cpu", Aidge::FCImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_FCIMPL_H_ */
