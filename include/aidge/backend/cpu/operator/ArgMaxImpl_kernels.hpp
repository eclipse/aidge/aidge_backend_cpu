/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_ARGMAXIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_ARGMAXIMPL_KERNELS_H_

#include <algorithm>   // std::for_each
#include <cstddef>     // std::size_t
#include <cstdint>     // std::int32_t
#include <functional>  //std::multiplies
#include <numeric>     //std::accumulate
#include <vector>
#include <limits>

#include "aidge/backend/cpu/operator/ArgMaxImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/operator/ArgMax.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {
template <class I, class O>
void ArgMaxImpl_cpu_forward_kernel(std::int32_t axis_,
                                    DimSize_t select_last_index,
                                    const std::vector<DimSize_t>& inputDims,
                                    const void* input_,
                                    void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    const std::size_t axis = static_cast<std::size_t>(axis_);

    std::size_t stride_post = 1;
    for (std::size_t i = axis + 1; i < inputDims.size(); ++i) {
        stride_post *= inputDims[i];
    }
    std::size_t stride_pre = 1;
    for (std::size_t i = 0; i < axis; ++i) {
        stride_pre *= inputDims[i];
    }
    const std::size_t dim_i = inputDims[axis];
    for (std::size_t pre = 0; pre < stride_pre; ++pre) {
        for (std::size_t post = 0; post < stride_post; ++post) {
            const std::size_t idx_i = pre * dim_i * stride_post + post;
            const std::size_t idx_o = pre * stride_post + post;
            I max = std::numeric_limits<I>::min();
            for (std::size_t i = 0; i < dim_i; ++i) {
                I curr_value = input[idx_i + i*stride_post];
                if (select_last_index) {
                    if (curr_value>=max) {
                        output[idx_o] = i;
                        max = curr_value;
                    }
                }
                else {
                    if (curr_value > max) {
                        output[idx_o] = i;
                        max = curr_value;
                    }
                }
            }
        }
    }

}

// Kernels registration to implementation entry point
REGISTRAR(ArgMaxImpl_cpu,
    {DataType::Float32},
    {ProdConso::defaultModel, Aidge::ArgMaxImpl_cpu_forward_kernel<float, float>, nullptr});
REGISTRAR(ArgMaxImpl_cpu,
    {DataType::Float64},
    {ProdConso::defaultModel, Aidge::ArgMaxImpl_cpu_forward_kernel<double, double>, nullptr});
REGISTRAR(ArgMaxImpl_cpu,
    {DataType::Int32},
    {ProdConso::defaultModel, Aidge::ArgMaxImpl_cpu_forward_kernel<std::int32_t, std::int32_t>, nullptr});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_ARGMAXIMPL_KERNELS_H_ */
