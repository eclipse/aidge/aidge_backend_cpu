/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SLICEIMPL_H__
#define AIDGE_CPU_OPERATOR_SLICEIMPL_H__

#include <memory>
#include <vector>
#include <array>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Slice.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// Operator implementation entry point for the backend
using SliceImpl_cpu = OperatorImpl_cpu<Slice_Op,
    void(const std::vector<std::int64_t>&,
                            const std::vector<std::int64_t>&,
                            const std::vector<std::int8_t>&,
                            const std::vector<std::int64_t>&,
                            const std::vector<DimSize_t>&,
                            const void*,
                            void*)>;

// Implementation entry point registration to Operator
REGISTRAR(Slice_Op, "cpu", Aidge::SliceImpl_cpu::create);
}  // namespace Aidge

#endif /* __AIDGE_CPU_OPERATOR_SLICEIMPL_H__ */
