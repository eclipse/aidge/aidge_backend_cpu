/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_ABSIMPL_H_
#define AIDGE_CPU_OPERATOR_ABSIMPL_H_

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Abs.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include <memory>
#include <vector>

namespace Aidge {
// Operator implementation entry point for the backend
using AbsImpl_cpu = OperatorImpl_cpu<Abs_Op,
    void(const std::size_t, const void*, void*)>;

// Implementation entry point registration to Operator
REGISTRAR(Abs_Op, "cpu", Aidge::AbsImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_ABSIMPL_H_ */
