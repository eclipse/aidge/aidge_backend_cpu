/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_AVGPOOLINGIMPL_H_
#define AIDGE_CPU_OPERATOR_AVGPOOLINGIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// Operator implementation entry point for the backend
using AvgPooling2D_Op = AvgPooling_Op<2>;
using AvgPoolingImpl2D_cpu = OperatorImpl_cpu<AvgPooling_Op<2>,
    void(const std::array<DimSize_t, 2>&,
        const std::array<DimSize_t, 2>&,
        const std::array<DimSize_t, 4>&,
        const void *,
        void *)>;

// Implementation entry point registration to Operator
REGISTRAR(AvgPooling2D_Op, "cpu", Aidge::AvgPoolingImpl2D_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_AVGPOOLINGIMPL_H_ */
