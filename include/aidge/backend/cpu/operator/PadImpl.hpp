/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_PADIMPL_H_
#define AIDGE_CPU_OPERATOR_PADIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
class Pad_ProdConso_cpu : public ProdConso {
public:
    Pad_ProdConso_cpu(const Operator& op): ProdConso(op) {}

    static std::unique_ptr<ProdConso> defaultModel(const Operator& op) {
        return std::make_unique<Pad_ProdConso_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
};

// Operator implementation entry point for the backend
using Pad1D_Op = Pad_Op<1>;
using PadImpl1D_cpu = OperatorImpl_cpu<Pad_Op<1>,
    void(const std::array<DimSize_t, 2>&,
                            const PadBorderType,
                            const double,
                            const std::array<DimSize_t, 3> &,
                            const void *,
                            void *)>;

using Pad2D_Op = Pad_Op<2>;
using PadImpl2D_cpu = OperatorImpl_cpu<Pad_Op<2>,
    void(const std::array<DimSize_t, 4>&,
                            const PadBorderType,
                            const double,
                            const std::array<DimSize_t, 4> &,
                            const void *,
                            void *)>;

// Implementation entry point registration to Operator
REGISTRAR(Pad1D_Op, "cpu", Aidge::PadImpl1D_cpu::create);
REGISTRAR(Pad2D_Op, "cpu", Aidge::PadImpl2D_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_PADIMPL_H_ */
