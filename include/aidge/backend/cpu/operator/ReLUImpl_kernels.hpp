/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_RELUIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_RELUIMPL_KERNELS_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/backend/cpu/operator/ReLUImpl.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// Kernels
template <class I, class O>
void ReLUImpl_cpu_forward_kernel(std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

//#pragma omp parallel for if (inputLenght > 1024)
    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = (input[i] > 0) ? input[i] : 0;
    }
}

template <class I, class GI, class GO>
void ReLUImpl_cpu_backward_kernel(const std::size_t inputLenght,
                                  const void* input_, const void* grad_output_,
				  void* grad_input_) {
    const I* input = static_cast<const I*>(input_);
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);
    for (std::size_t i = 0; i < inputLenght; ++i) {
        grad_input[i] = (input[i] > 0) ? grad_output[i] : 0;
    }
}

// Kernels registration to implementation entry point
REGISTRAR(ReLUImpl_cpu,
    {DataType::Float32},
    {ProdConso::inPlaceModel, Aidge::ReLUImpl_cpu_forward_kernel<float, float>, Aidge::ReLUImpl_cpu_backward_kernel<float, float, float>});
REGISTRAR(ReLUImpl_cpu,
    {DataType::Float64},
    {ProdConso::inPlaceModel, Aidge::ReLUImpl_cpu_forward_kernel<double, double>, Aidge::ReLUImpl_cpu_backward_kernel<double, double, double>});
REGISTRAR(ReLUImpl_cpu,
    {DataType::Int32},
    {ProdConso::inPlaceModel, Aidge::ReLUImpl_cpu_forward_kernel<int32_t, int32_t>, Aidge::ReLUImpl_cpu_backward_kernel<int32_t, int32_t, int32_t>});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_RELUIMPL_KERNELS_H_ */
