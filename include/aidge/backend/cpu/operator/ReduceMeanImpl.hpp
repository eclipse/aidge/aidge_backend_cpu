/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_H_
#define AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// Operator implementation entry point for the backend
using ReduceMeanImpl_cpu = OperatorImpl_cpu<ReduceMean_Op,
    void(const std::vector<std::int32_t>&,
                            DimSize_t,
                            const std::vector<DimSize_t>&,
                            const void *,
                            void *)>;

// Implementation entry point registration to Operator
REGISTRAR(ReduceMean_Op, "cpu", Aidge::ReduceMeanImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_H_ */
