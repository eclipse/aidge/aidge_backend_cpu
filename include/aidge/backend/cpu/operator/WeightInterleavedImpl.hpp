/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_WEIGHTINTERLEAVINGIMPL_H_
#define AIDGE_CPU_OPERATOR_WEIGHTINTERLEAVINGIMPL_H_

#include <array>
#include <memory>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/WeightInterleaving.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// Operator implementation entry point for the backend
using WeightInterleavedImpl_cpu = OperatorImpl_cpu<WeightInterleaving_Op,
    void(const DimSize_t,
        const DimSize_t,
        const DimSize_t,
        const void *,
        void *)>;

// Implementation entry point registration to Operator
REGISTRAR(WeightInterleaving_Op, "cpu", Aidge::WeightInterleavedImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_WeightInterleavingIMPL_H_ */
