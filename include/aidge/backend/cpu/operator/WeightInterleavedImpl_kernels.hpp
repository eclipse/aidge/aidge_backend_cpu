/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_WEIGHTINTERLEAVEDIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_WEIGHTINTERLEAVEDIMPL_KERNELS_H_

#include <cstddef>  // std::size_t
#include <cstdint>  // std::int8_t, std::uint8_t

#include "aidge/backend/cpu/operator/WeightInterleavedImpl.hpp"
#include "aidge/data/DataType.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/ErrorHandling.hpp"


namespace Aidge {

    /**
     * @brief Compacts 8-bit data into a smaller bit-width representation.
     *
     * This function takes an array of 8-bit data and compacts it into smaller chunks
     * based on the specified bit-width `nb_bits`. Each element in `compactData` will
     * store multiple packed `nb_bits` segments extracted from `data`.
     *
     * @param data The input array of 8-bit values to be compacted.
     * @param dataSize The size of the input `data` array.
     * @param compactData The output array storing the compacted data.
     * @param nb_bits The number of bits to extract from each `data` element (must be less than 8).
     */
    template <typename T>
    void compact_data(const T* data, std::size_t dataSize, T* compactData, std::uint8_t nb_bits) {
        AIDGE_ASSERT(nb_bits > 0 && nb_bits < 5, "Cannot compact with the given nb_bits"); // Ensure valid bit width

        // Mask to extract `nb_bits` from each data element
        const unsigned int mask = (1U << nb_bits) - 1;

        // Calculate the number of `nb_bits` segments that fit into an 8-bit compacted value
        const unsigned int nbSlot = 8 / nb_bits;

        // Case nb_bits=3 or 4, then shift is 4
        // Case nb_bits=2, then shift is 2
        // Case nb_bits=1, then shift is 1
        std::uint8_t shift = 8 / nbSlot;

        const unsigned int nbFullCompactbytes = dataSize / nbSlot;

        // Main loop to process data in groups of `nbSlot`
        for (std::size_t i = 0; i < nbFullCompactbytes; ++i) {
            T compact = 0;

            for (unsigned int j = 0; j < nbSlot; ++j) {
                compact |= (data[i * nbSlot + j] & mask);    // Apply mask to keep `nb_bits` only

                // Shift only if not on the last slot to make room for the next `nb_bits`
                if (j < nbSlot - 1) {
                    compact <<= shift;
                }
            }
            // Store the compacted value in the output array
            compactData[i] = compact;
        }


        // Handle any remaining data elements (if dataSize is not a multiple of nbSlot).
        std::size_t remaining = dataSize % nbSlot;
        if (remaining != 0) {
            std::int8_t compact = 0;
            for (std::size_t j = 0; j < remaining; ++j) {
                compact |= (data[nbFullCompactbytes*nbSlot + j] & mask);

                if (j < remaining - 1) {
                    compact <<= shift;
                }
            }
            compact <<= (shift*(nbSlot - remaining));
            // Store the last compacted value
            compactData[dataSize / nbSlot] = compact;
        }
    }

template <class I, class O, int nb_bits>
void WeightInterleavedImpl_cpu_forward_kernel(const DimSize_t input_interleaving,
                            const DimSize_t nb_interleaving,
                            const DimSize_t output_interleaving,
                            const void* input_,
                            void* output_) {
    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    // Aidge::compact_data(const std::int8_t* data, std::size_t dataSize, std::int8_t* compactData, std::uint8_t nb_bits) {
    for (std::size_t i=0; i<nb_interleaving; ++i){
        compact_data(input+(i*input_interleaving), input_interleaving, output+(i*output_interleaving), static_cast<std::uint8_t>(nb_bits));
    }

}


REGISTRAR(WeightInterleavedImpl_cpu,
    {ImplSpec::IOSpec{DataType::Int4, DataFormat::NHWC}, ImplSpec::IOSpec{WeightInterleavedType_v<DataType::Int4>, DataFormat::NHWC}},
    {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<int8_t, int8_t, 4>, nullptr});
REGISTRAR(WeightInterleavedImpl_cpu,
    {ImplSpec::IOSpec{DataType::Int3, DataFormat::NHWC}, ImplSpec::IOSpec{WeightInterleavedType_v<DataType::Int3>, DataFormat::NHWC}},
    {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<int8_t, int8_t, 3>, nullptr});
REGISTRAR(WeightInterleavedImpl_cpu,
    {ImplSpec::IOSpec{DataType::Int2, DataFormat::NHWC}, ImplSpec::IOSpec{WeightInterleavedType_v<DataType::Int2>, DataFormat::NHWC}},
    {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<int8_t, int8_t, 2>, nullptr});
REGISTRAR(WeightInterleavedImpl_cpu,
    {ImplSpec::IOSpec{DataType::Binary, DataFormat::NHWC}, ImplSpec::IOSpec{WeightInterleavedType_v<DataType::Binary>, DataFormat::NHWC}},
    {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<int8_t, int8_t, 1>, nullptr});

REGISTRAR(WeightInterleavedImpl_cpu,
    {ImplSpec::IOSpec{DataType::UInt4, DataFormat::NHWC}, ImplSpec::IOSpec{WeightInterleavedType_v<DataType::UInt4>, DataFormat::NHWC}},
    {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<uint8_t, uint8_t, 4>, nullptr});
REGISTRAR(WeightInterleavedImpl_cpu,
    {ImplSpec::IOSpec{DataType::UInt3, DataFormat::NHWC}, ImplSpec::IOSpec{WeightInterleavedType_v<DataType::UInt3>, DataFormat::NHWC}},
    {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<uint8_t, uint8_t, 3>, nullptr});
REGISTRAR(WeightInterleavedImpl_cpu,
    {ImplSpec::IOSpec{DataType::UInt2, DataFormat::NHWC}, ImplSpec::IOSpec{WeightInterleavedType_v<DataType::UInt2>, DataFormat::NHWC}},
    {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<uint8_t, uint8_t, 2>, nullptr});


// REGISTRAR(WeightInterleavedImpl_cpu,
//     {ImplSpec::IOSpec{DataType::Int4, DataFormat::NHWC}},
//     {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<int8_t, int8_t, 4>, nullptr});
// REGISTRAR(WeightInterleavedImpl_cpu,
//     {ImplSpec::IOSpec{DataType::Int3, DataFormat::NHWC}},
//     {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<int8_t, int8_t, 3>, nullptr});
// REGISTRAR(WeightInterleavedImpl_cpu,
//     {ImplSpec::IOSpec{DataType::Int2, DataFormat::NHWC}},
//     {ProdConso::defaultModel, Aidge::WeightInterleavedImpl_cpu_forward_kernel<int8_t, int8_t, 2>, nullptr});


}

#endif /* AIDGE_CPU_OPERATOR_WEIGHTINTERLEAVEDIMPL_KERNELS_H_ */