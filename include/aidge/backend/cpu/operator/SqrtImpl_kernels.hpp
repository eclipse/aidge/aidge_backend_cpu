/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SQRTIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_SQRTIMPL_KERNELS_H_

#include <cmath>    // std::sqrt
#include <cstddef>  // std::size_t

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/SqrtImpl.hpp"

namespace Aidge {
template <class I, class O>
void SqrtImpl_cpu_forward_kernel(const std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = static_cast<O>(std::sqrt(static_cast<float>(input[i])));
    }
}

template <class I, class O>
void SqrtImpl_cpu_backward_kernel(const std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = static_cast<O>(0.5/(std::sqrt(static_cast<float>(input[i]))));
    }
}

REGISTRAR(SqrtImpl_cpu,
    {DataType::Float32},
    {ProdConso::inPlaceModel, Aidge::SqrtImpl_cpu_forward_kernel<float, float>, Aidge::SqrtImpl_cpu_backward_kernel<float, float>});
REGISTRAR(SqrtImpl_cpu,
    {DataType::Float64},
    {ProdConso::inPlaceModel, Aidge::SqrtImpl_cpu_forward_kernel<double, double>, Aidge::SqrtImpl_cpu_backward_kernel<double, double>});
REGISTRAR(SqrtImpl_cpu,
    {DataType::Int32},
    {ProdConso::inPlaceModel, Aidge::SqrtImpl_cpu_forward_kernel<int32_t, int32_t>, Aidge::SqrtImpl_cpu_backward_kernel<int32_t, int32_t>});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SQRTIMPL_KERNELS_H_ */
