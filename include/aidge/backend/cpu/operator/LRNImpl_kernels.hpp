/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_LRNIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_LRNIMPL_KERNELS_H_

#include "aidge/utils/Registrar.hpp"
#include <cstddef>
#include <cmath>
#include "aidge/data/Data.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

#include "aidge/backend/cpu/operator/LRNImpl.hpp"

namespace Aidge {
template <class I, class O>
void LRNImpl_cpu_forward_kernel(float alpha, float beta, float bias, std::size_t size, const std::vector<DimSize_t>& inputDims, const void* input_, void* output_)
{
    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    const DimSize_t nbBatch = inputDims[0];
    const DimSize_t nbChannels = (inputDims.size() > 1) ? inputDims[1] : 1;
    const DimSize_t featureMapSize = (inputDims.size() > 2) ? std::accumulate(inputDims.begin() + 2, inputDims.end(), 1, std::multiplies<DimSize_t>()) : 1;

    for (std::size_t batch = 0; batch < nbBatch; ++batch) {
        for (std::size_t ch = 0; ch < nbChannels; ++ch) {
            const std::size_t ioIndex = (ch + batch*nbChannels) * featureMapSize;
            const unsigned int channelMin
                = std::max<int>(0, ch - size / 2);
            const unsigned int channelMax
                = std::min<size_t>(nbChannels - 1, ch + size / 2);

            for (std::size_t feature = 0; feature<featureMapSize; ++feature) {
                // For each input channel, accumulate the value
                O accAccrossChannels(0.0);

                for (unsigned int accChannel = channelMin;
                    accChannel < channelMax; ++accChannel)
                {
                    accAccrossChannels += input[ioIndex + feature];
                }

                // Compute the output signal
                output[ioIndex + feature] = input[ioIndex + feature]
                    / std::pow((bias + (accAccrossChannels * accAccrossChannels) * alpha), beta);
            }
        }
    }
}

REGISTRAR(LRNImpl_cpu,
    {DataType::Float32},
    {ProdConso::inPlaceModel, Aidge::LRNImpl_cpu_forward_kernel<float, float>, nullptr});
REGISTRAR(LRNImpl_cpu,
    {DataType::Float64},
    {ProdConso::inPlaceModel, Aidge::LRNImpl_cpu_forward_kernel<double, double>, nullptr});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_LRNIMPL_KERNELS_H_ */
