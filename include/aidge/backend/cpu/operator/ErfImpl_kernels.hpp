/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_ERFIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_ERFIMPL_KERNELS_H_

#include <cmath>

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/ErfImpl.hpp"

namespace Aidge {
template <class I, class O>
void ErfImpl_cpu_forward_kernel(std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = std::erf(input[i]);
    }
}

// Kernels registration to implementation entry point
REGISTRAR(ErfImpl_cpu,
    {DataType::Float32},
    {ProdConso::inPlaceModel, Aidge::ErfImpl_cpu_forward_kernel<float, float>, nullptr});
REGISTRAR(ErfImpl_cpu,
    {DataType::Float64},
    {ProdConso::inPlaceModel, Aidge::ErfImpl_cpu_forward_kernel<double, double>, nullptr});
REGISTRAR(ErfImpl_cpu,
    {DataType::Int32},
    {ProdConso::inPlaceModel, Aidge::ErfImpl_cpu_forward_kernel<std::int32_t, std::int32_t>, nullptr});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_ERFIMPL_KERNELS_H_ */
