/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_LNIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_LNIMPL_KERNELS_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/LnImpl.hpp"

namespace Aidge {
template <class I, class O>
void LnImpl_cpu_forward_kernel(std::size_t inputLenght,
                               const void* input_,
                               void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);
	const float eps = 1.0e-20f;

//#pragma omp parallel for if (inputLenght > 1024)
    for (std::size_t i = 0; i < inputLenght; ++i) {
		if (input[i] > I(eps)) {
			output[i] = std::log(input[i]);
		} else {
			output[i] = std::log(I(eps));
		}
    }
}

template <class I, class GI, class GO>
void LnImpl_cpu_backward_kernel(const std::size_t inputLenght,
                                const void* input_, const void* grad_output_,
	                            void* grad_input_) {
						 
    const I* input = static_cast<const I*>(input_);
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);
	const float eps = 1.0e-20f;
	
    for (std::size_t i = 0; i < inputLenght; ++i) {
		if (input[i] > I(eps)) {
			grad_input[i] = grad_output[i] / input[i];
		} else {
			grad_input[i] = GI(0);
		}
    }
}

// Kernels registration to implementation entry point
REGISTRAR(LnImpl_cpu,
    {DataType::Float32},
    {ProdConso::inPlaceModel, Aidge::LnImpl_cpu_forward_kernel<float, float>, Aidge::LnImpl_cpu_backward_kernel<float, float, float>});
REGISTRAR(LnImpl_cpu,
    {DataType::Float64},
    {ProdConso::inPlaceModel, Aidge::LnImpl_cpu_forward_kernel<double, double>, Aidge::LnImpl_cpu_backward_kernel<double, double, double>});
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_LNIMPL_KERNELS_H_ */
