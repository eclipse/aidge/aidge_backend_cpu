/********************************************************************************
 * Copyright (c) 2025 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_HEAVISIDEIMPL_H_
#define AIDGE_CPU_OPERATOR_HEAVISIDEIMPL_H_

#include <cstddef> // std::size_t

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Heaviside.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/future_std/span.hpp"

namespace Aidge {
using HeavisideImplCpu =
    OperatorImpl_cpu<Heaviside_Op,
                     void(std::size_t, const void *, void *, const float),
                     void(const float, std::size_t, const void *, void *)>;

// Implementation entry point registration for operator Heaviside
REGISTRAR(Heaviside_Op, "cpu", HeavisideImplCpu::create);
} // namespace Aidge

#endif // AIDGE_CPU_OPERATOR_HEAVISIDEIMPL_H_
