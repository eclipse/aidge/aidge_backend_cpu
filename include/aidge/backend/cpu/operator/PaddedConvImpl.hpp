/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_PADDEDCONVIMPL_H_
#define AIDGE_CPU_OPERATOR_PADDEDCONVIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/MetaOperatorDefs.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// Operator implementation entry point for the backend
using PaddedConv1D_Op = MetaOperator_Op;
using PaddedConvImpl1D_cpu = OperatorImpl_cpu<MetaOperator_Op,
    void(const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 1>&,
                            const std::array<DimSize_t, 1>&,
                            const std::array<DimSize_t, 1>&,
                            const std::array<DimSize_t, 3> &,
                            DimSize_t,
                            const void *,
                            const void *,
                            const void *,
                            void *)>;

using PaddedConv2D_Op = MetaOperator_Op;
using PaddedConvImpl2D_cpu = OperatorImpl_cpu<MetaOperator_Op,
    void(const std::array<DimSize_t, 4>&,
                            const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 4> &,
                            DimSize_t,
                            const void *,
                            const void *,
                            const void *,
                            void *)>;

// Implementation entry point registration to Operator
// Uncomment to activate implementation for PaddedConv. It is currently less efficient, hence why it is commented.
// REGISTRAR(PaddedConv1D_Op, std::array<std::string, 2>({"cpu", "PaddedConv1D"}), Aidge::PaddedConvImpl1D_cpu::create);
// REGISTRAR(PaddedConv2D_Op, std::array<std::string, 2>({"cpu", "PaddedConv2D"}), Aidge::PaddedConvImpl2D_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_PADDEDCONVIMPL_H_ */
