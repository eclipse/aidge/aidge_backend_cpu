/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_RESIZEIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_RESIZEIMPL_FORWARD_KERNEL_H_

#include "aidge/backend/cpu/operator/ResizeImpl.hpp"

#include <aidge/data/Data.hpp>
#include <aidge/data/half.hpp>
#include <aidge/operator/Pad.hpp>
#include <cmath>
#include <cstdint>
#include <numeric>

#include "aidge/backend/cpu/data/Interpolation.hpp"
#include "aidge/data/Interpolation.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

template <typename IO>
void ResizeImpl_cpu_forward_kernel(
    const void *input_,
    const std::vector<DimSize_t> &inputDims,
    const std::vector<DimSize_t> &outputDims,
    const Interpolation::CoordinateTransformation coordTransfoMode,
    const Interpolation::Mode interpMode,
    const PadBorderType paddingMode,
    // const double * /*roi*/,
    // const float * /*scales*/,
    // const int64_t * /*sizes*/,
    void *output_) {

    // Seting a data
    const IO *input = static_cast<const IO *>(input_);
    IO *output = static_cast<IO *>(output_);

    const DimSize_t outputLen = std::accumulate(outputDims.cbegin(),
                                          outputDims.cend(),
                                          1,
                                          std::multiplies<DimSize_t>());
    std::vector<float> coordInApprox(inputDims.size());
    std::vector<std::size_t> coordIn(inputDims.size());
    std::vector<DimSize_t> coordOut;
    for (DimSize_t idxFlatOut = 0; idxFlatOut < outputLen; ++idxFlatOut) {
        coordOut = Tensor::toCoord(outputDims, idxFlatOut);
        coordInApprox =
            Interpolation::untransformCoordinates(coordOut,
                                                  inputDims,
                                                  outputDims,
                                                  coordTransfoMode);
        if ((interpMode == Interpolation::Mode::Ceil) || (interpMode == Interpolation::Mode::Floor) || (interpMode == Interpolation::Mode::RoundPreferCeil) || (interpMode == Interpolation::Mode::RoundPreferFloor)) {
            for (std::size_t i = 0; i < coordInApprox.size(); ++i) {
                if (interpMode == Interpolation::Mode::Ceil) {
                    coordInApprox[i] = std::ceil(coordInApprox[i]);
                } else if (interpMode == Interpolation::Mode::Floor) {
                    coordInApprox[i] = std::floor(coordInApprox[i]);
                } else if (interpMode == Interpolation::Mode::RoundPreferCeil) {
                    coordInApprox[i] = std::floor(coordInApprox[i] + 0.5f);
                } else { // (interpMode == Interpolation::Mode::RoundPreferFloor)
                    coordInApprox[i] = std::ceil(coordInApprox[i] - 0.5f);
                }
            }
            if (Tensor::isInBounds<float>(inputDims, coordInApprox)) {
                for (std::size_t i = 0; i < coordInApprox.size(); ++i) {
                    coordIn[i] = static_cast<std::size_t>(coordInApprox[i]);
                }
            } else {
                if (paddingMode == PadBorderType::Edge) {
                    for (std::size_t i = 0; i < coordInApprox.size(); ++i) {
                        coordIn[i] = coordInApprox[i] < 0 ? 0 : (coordInApprox[i] >=inputDims[i] ? inputDims[i] - 1 : static_cast<std::size_t>(coordInApprox[i]));
                    }
                } else {
                    AIDGE_THROW_OR_ABORT(std::runtime_error, "Padding mode not supported");
                }
            }
            output[idxFlatOut] = input[Tensor::toIndex(inputDims, coordIn)];
        } else {
            std::set<Interpolation::Point<IO>> neighbours =
                InterpolationCPU::retrieveNeighbours(input,
                                                     inputDims,
                                                     coordInApprox,
                                                     paddingMode);
            output[idxFlatOut] = InterpolationCPU::interpolate(coordInApprox,
                                                               neighbours,
                                                               interpMode);
        }
    }
    return;
}

// Kernels registration to implementation entry point
REGISTRAR(ResizeImpl_cpu,
          {{{DataType::Int16},
            {DataType::Any},
            {DataType::Any},
            {DataType::Any}},
           {DataType::Int16}},
          {ProdConso::inPlaceModel,
           ResizeImpl_cpu_forward_kernel<int16_t>,
           nullptr});
REGISTRAR(ResizeImpl_cpu,
          {{{DataType::Int32},
            {DataType::Any},
            {DataType::Any},
            {DataType::Any}},
           {DataType::Int32}},
          {ProdConso::inPlaceModel,
           ResizeImpl_cpu_forward_kernel<int32_t>,
           nullptr});
REGISTRAR(ResizeImpl_cpu,
          {{{DataType::Int64},
            {DataType::Any},
            {DataType::Any},
            {DataType::Any}},
           {DataType::UInt64}},
          {ProdConso::inPlaceModel,
           ResizeImpl_cpu_forward_kernel<int64_t>,
           nullptr});

REGISTRAR(ResizeImpl_cpu,
          {{{DataType::Float16},
            {DataType::Any},
            {DataType::Any},
            {DataType::Any}},
           {DataType::Float16}},
          {ProdConso::inPlaceModel,
           ResizeImpl_cpu_forward_kernel<half_float::half>,
           nullptr});
REGISTRAR(ResizeImpl_cpu,
          {{{DataType::Float32},
            {DataType::Any},
            {DataType::Any},
            {DataType::Any}},
           {DataType::Float32}},
          {ProdConso::inPlaceModel,
           ResizeImpl_cpu_forward_kernel<float>,
           nullptr});
REGISTRAR(ResizeImpl_cpu,
          {{{DataType::Float64},
            {DataType::Any},
            {DataType::Any},
            {DataType::Any}},
           {DataType::Float64}},
          {ProdConso::inPlaceModel,
           ResizeImpl_cpu_forward_kernel<double>,
           nullptr});
} // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_RESIZEIMPL_FORWARD_KERNEL_H_ */
