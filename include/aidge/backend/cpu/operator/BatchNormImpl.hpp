/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_BATCHNORMIMPL_H_
#define AIDGE_CPU_OPERATOR_BATCHNORMIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/BatchNorm.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// Operator implementation entry point for the backend
using BatchNorm2D_Op = BatchNorm_Op<2>;
using BatchNormImpl2D_cpu = OperatorImpl_cpu<BatchNorm_Op<2>,
    void(float,
        float,
        const std::vector<DimSize_t> &,
        const void *,
        const void *,
        const void *,
        void *,
        void *,
        void *,
        const bool)>;

// Implementation entry point registration to Operator
REGISTRAR(BatchNorm2D_Op, "cpu", Aidge::BatchNormImpl2D_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_BATCHNORMIMPL_H_ */
