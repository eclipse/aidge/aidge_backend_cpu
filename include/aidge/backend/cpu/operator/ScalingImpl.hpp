/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef __AIDGE_CPU_OPERATOR_ScalingIMPL_H__
#define __AIDGE_CPU_OPERATOR_ScalingIMPL_H__

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/Scaling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <memory>
#include <vector>
#include <array>

namespace Aidge {
// Operator implementation entry point for the backend
using ScalingImpl_cpu = OperatorImpl_cpu<Scaling_Op,
    void(const float,
        const std::size_t,
        const bool,
        std::size_t,
        const void*,
        void*)>;

// Implementation entry point registration to Operator
REGISTRAR(Scaling_Op, "cpu", Aidge::ScalingImpl_cpu::create);
}  // namespace Aidge

#endif /* __AIDGE_CPU_OPERATOR_ScalingIMPL_H__ */