/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_DATA_INTERPOLATION_H_
#define AIDGE_CPU_DATA_INTERPOLATION_H_

#include <vector>

#include <aidge/data/Interpolation.hpp>
#include <aidge/utils/Types.h>

namespace Aidge {
class InterpolationCPU : public Interpolation {
  public:
    /*
     * @brief Interpolates values given via input in given mode.
     *
     * Values are contiguously arranged in a "square" shape around the point to
     * interpolate. Depending on interpolation mode.
     * The point that will be interpolated is located right in the
     * middle of all points.
     * Immediate neighbours :
     * 1D interp :     2D interp :
     *                 . . . . . .
     * . . 1 2 . .     . . . . . .
     *                 . . 1 2 . .
     *                 . . 3 4 . .
     *                 . . . . . .
     *                 . . . . . .
     *
     * 2 neighbours :
     * 1D interp :         2D interp :
     *                   .  .  .  .  .  .  . .
     *                   .  .  .  .  .  .  . .
     * . . 1 2 3 4 . .   .  .  1  2  3  4  . .
     *                   .  .  5  6  7  8  . .
     *                   .  .  9 10 11 12  . .
     *                   .  . 13 14 15 16  . .
     *                   .  .  .  .  .  .  . .
     *                   .  .  .  .  .  .  . .
     *
     * @param[in] originalCoords: coord of the point to interpolate in the
     * original picture. These coords are generated with
     * Interpolation::untransformCoords(coordsInInterpolatedTensor)
     * @param[in] points : points to interpolate, arranged in a vector of a
     * pairs ((point_coord), value) :
     * [[[X1, X2, ..., XN], Xval], ...., [[A1, A2, ..., AN],Aval]].
     * With :
     * - N: the number of dimensions.
     * - A: the number of points of the grid to interpolate.
     * - All coordinates expressed in originalTensor frame.
     * @param[in] interpMode: interpolation mode
     * @return interpolated value
     */
    template <typename T>
    static T interpolate(const std::vector<float> &coordsToInterpolate,
                         const std::set<Point<T>> &points,
                         const Mode interpMode = Interpolation::Mode::Linear);

    /**
     * @brief performs linear interpolation on given points.
     * @param[in] values: values to interpolate, since we only do an average of
     * all values, their indexes isn't useful.
     * @return interpolated value
     */
    template <typename T>
    static T linear(const std::vector<float> &originalCoords,
                    const std::set<Point<T>> &points);

    /**
     * @brief performs nearest interpolation on given points.
     * @note it is a wrapper for linearRecurse() private method
     * @param[in] coordsToInterpolate: coordinates to interpolate
     * @param[in] points: points to interpolate
     * @param[in] interpMode: interpolation method, must be a Nearest...
     * otherwise function will throw an error.
     * @return interpolated value
     */
    template <typename T>
    static T nearest(const std::vector<float> &coordsToInterpolate,
                     const std::set<Point<T>> &points,
                     const Interpolation::Mode nearestMode);

  private:
    /**
     * @brief actual linear interpolation function.
     * will :
     * - Split all points along each dimension depending of if their coords at
     * idx alongDim are above or under coordsToInterpolate until they are
     * 1-to-1.
     * - Perform interpolation in 2 leftover points and return interpolated
     * point to parent call with a set of size 1.
     * - repeat until all dimensions have been interpolated.
     * @param[in] coordsToInterpolate: coordinates to interpolate
     * @param[in] points: points to interpolate
     * @param[in] alongDim: discriminant on along which dimension are being
     * segregated.
     * @return
     */
    template <typename T>
    static std::set<Interpolation::Point<T>>
    linearRecurse(const std::vector<float> &coordsToInterpolate,
                  const std::set<Point<T>> &points,
                  const DimIdx_t alongDim = 0);
};

} // namespace Aidge

#endif // AIDGE_CPU_DATA_INTERPOLATION_H_
