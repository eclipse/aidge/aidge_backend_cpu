/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <chrono>      // std::micro, std::chrono::time_point,
                       // std::chrono::system_clock
#include <cstddef>     // std::size_t
#include <cstdint>     // std::int64_t, std::uint16_t
#include <memory>
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_random.hpp>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/ConstantOfShapeImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/filler/Filler.hpp"
#include "aidge/operator/ConstantOfShape.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/TensorUtils.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
TEST_CASE("[cpu/operator] ConstantOfShape", "[ConstantOfShape][CPU]") {
  constexpr std::uint16_t NBTRIALS = 10;
  // Create a random number generator
  auto random_seed = Catch::Generators::Detail::getSeed;
  std::mt19937 gen(random_seed());
  std::uniform_real_distribution<float> valueDist(
      0.1f, 1.1f); // Random float distribution between 0 and 1
  std::uniform_int_distribution<DimSize_t> input_tensor_size_dist(
      std::size_t(1), std::size_t(10));
  std::uniform_int_distribution<int64_t> input_tensor_values_dist(
      std::size_t(1), std::size_t(7));
  std::uniform_real_distribution<double> operator_attr_value_dist(-100., 100.);

  ///////////////////////////////////////////////
  // SETUP FUNCTIONS
  auto generate_input_tensor =
      [&gen, &input_tensor_size_dist,
       &input_tensor_values_dist]() -> std::shared_ptr<Tensor> {
    std::vector<DimSize_t> input_dims;
    input_dims.push_back(input_tensor_size_dist(gen));

    auto result = std::make_shared<Tensor>(input_dims);
    result->setDataType(DataType::Int64);
    result->setBackend("cpu");
    for (DimSize_t i = 0; i < result->size(); ++i) {
      result->set<std::int64_t>(i, input_tensor_values_dist(gen));
    }
    return result;
  };

  auto generate_random_operator =
      [&gen,
       &operator_attr_value_dist]() -> std::shared_ptr<ConstantOfShape_Op> {
    auto node = ConstantOfShape(Tensor(operator_attr_value_dist(gen)));
    auto op = std::static_pointer_cast<ConstantOfShape_Op>(node->getOperator());
    op->setDataType(DataType::Float64);
    op->setBackend("cpu");
    return op;
  };

  auto generate_output_tensor = [](std::shared_ptr<Tensor> input_tensor,
                                   std::shared_ptr<ConstantOfShape_Op> op) {
    std::vector<DimSize_t> output_dims;
    output_dims.reserve(input_tensor->size());
    for (DimSize_t i = 0; i < input_tensor->size(); ++i) {
      output_dims.push_back(input_tensor->get<int64_t>(i));
    }
    auto result = std::make_shared<Tensor>(output_dims);
    result->setDataType(op->value().dataType());
    result->setBackend("cpu");
    constantFiller(result, op->value().get<double>(0));
    return result;
  };

  /////////////////////////////////////
  // BENCHMARKING
  std::chrono::time_point<std::chrono::system_clock> start;
  std::chrono::time_point<std::chrono::system_clock> end;
  std::chrono::duration<double, std::micro> duration{};
  int number_of_operation{0};

  SECTION("ConstantOfShapeImpl_cpu::forward()") {
    for (int i = 0; i < NBTRIALS; ++i) {
      auto input_T = generate_input_tensor();
      std::shared_ptr<ConstantOfShape_Op> op = generate_random_operator();
      auto output_T = generate_output_tensor(input_T, op);
      op->associateInput(0, input_T);

      REQUIRE(op->forwardDims(true));
      REQUIRE_NOTHROW(op->forward());

      CHECK(output_T->nbDims() == op->getOutput(0)->nbDims());
      for (DimIdx_t i = 0; i < output_T->nbDims(); ++i) {
        CHECK(output_T->dims().at(i) == op->getOutput(0)->dims().at(i));
      }
      CHECK(approxEq<double>(*output_T, *op->getOutput(0)));
    }
  }
}
} // namespace Aidge

