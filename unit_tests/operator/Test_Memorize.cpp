/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <string>

#include <catch2/catch_test_macros.hpp>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/AddImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/Add.hpp"
#include "aidge/operator/Memorize.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/recipes/GraphViewHelper.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"

namespace Aidge {

TEST_CASE("[cpu/operator] Memorize(forward)", "[Memorize][CPU]") {
    SECTION("Test simple") {
        std::shared_ptr<Tensor> inputTensor =
                std::make_shared<Tensor>(Array1D<int, 1>{{1}});

        auto input = Producer({1}, "input");
        auto init = Producer({1}, "init");
        auto add = Add("add");
        auto mem = Memorize(3, "mem");

        input->addChild(add, 0, 0);
        init->addChild(mem, 0, 1);
        add->addChild(mem, 0,0);
        mem->addChild(/*otherNode=*/add, /*outId=*/1, /*otherInId=*/1);

        input->getOperator()->setOutput(0, inputTensor);
        init->getOperator()->setOutput(0, inputTensor);

        auto g = getConnectedGraphView(input);

        g->setDataType(Aidge::DataType::Int32);
        g->setBackend("cpu");
        g->forwardDims();
        g->save("simple_graph");

        SequentialScheduler scheduler(g);
        REQUIRE_NOTHROW(scheduler.forward());
        scheduler.saveSchedulingDiagram("simple");

        const Tensor expectedOutput = Array1D<int, 1>{{4}};
        std::shared_ptr<Tensor> other = std::static_pointer_cast<OperatorTensor>(mem->getOperator())->getOutput(0);
        other->print();
        REQUIRE((*other == expectedOutput));
    }
}
} // namespace Aidge
