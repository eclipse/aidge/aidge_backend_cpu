/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstddef> // std::size_t
#include <cstdint> // std::uint16_t
#include <memory>
#include <random>  // std::random_device, std::mt19937, std::uniform_int_distribution, std::uniform_real_distribution

#include <catch2/catch_test_macros.hpp>

#include "aidge/backend/cpu/operator/AndImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/And.hpp"
#include "aidge/utils/ArrayHelpers.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] And(forward)", "[And][CPU]") {
        SECTION("ForwardDims")
    {
        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(2), std::size_t(10));
        std::uniform_int_distribution<std::size_t> nbDimsDist(std::size_t(1), std::size_t(5));
        std::uniform_int_distribution<int> boolDist(0,1);

        SECTION("Same dimensions") {
            for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
                DimSize_t nbDims = nbDimsDist(gen);
                std::vector<DimSize_t> dims(nbDims);
                for (std::size_t i = 0; i < nbDims; i++) {
                    dims[i] = dimSizeDist(gen);
                }

                std::shared_ptr<Tensor> myInput1 = std::make_shared<Tensor>(dims);
                myInput1->setBackend("cpu");
                myInput1->setDataType(DataType::Float32);
                myInput1->zeros();
                std::shared_ptr<Tensor> myInput2 = std::make_shared<Tensor>(dims);
                myInput2->setBackend("cpu");
                myInput2->setDataType(DataType::Float32);
                myInput2->zeros();
                std::shared_ptr<Node> myAnd = And();
                auto op = std::static_pointer_cast<OperatorTensor>(myAnd -> getOperator());
                op->associateInput(0,myInput1);
                op->associateInput(1,myInput2);
                op->setDataType(DataType::Float32);
                op->setBackend("cpu");
                op->forwardDims();

                const auto outputDims = op->getOutput(0)->dims();
                REQUIRE(outputDims == dims);
            }
        }
        SECTION("Broadcasting") {
            for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
                DimSize_t nbDims = nbDimsDist(gen);
                std::vector<DimSize_t> dims1(nbDims, 1);
                std::vector<DimSize_t> dims2(nbDims, 1);
                std::vector<DimSize_t> expectedOutDims;
                for (std::size_t i = 0; i < nbDims; i++) {
                    DimSize_t dim = dimSizeDist(gen);
                    if (boolDist(gen)) {
                        dims1[i] = dim;
                    }
                    if (boolDist(gen)) {
                        dims2[i] = dim;
                    }
                    expectedOutDims.push_back(std::max(dims1[i],dims2[i]));
                }


                std::shared_ptr<Tensor> myInput1 = std::make_shared<Tensor>(dims1);
                myInput1->setBackend("cpu");
                myInput1->setDataType(DataType::Float32);
                myInput1->zeros();
                std::shared_ptr<Tensor> myInput2 = std::make_shared<Tensor>(dims2);
                myInput2->setBackend("cpu");
                myInput2->setDataType(DataType::Float32);
                myInput2->zeros();
                std::shared_ptr<Node> myAnd = And();
                auto op = std::static_pointer_cast<OperatorTensor>(myAnd -> getOperator());
                op->associateInput(0,myInput1);
                op->associateInput(1,myInput2);
                op->setDataType(DataType::Float32);
                op->setBackend("cpu");

                op->forwardDims();

                const auto outputDims = op->getOutput(0)->dims();
                REQUIRE(outputDims == expectedOutDims);
            }
        }
    }
    SECTION("Same size inputs") {
        std::shared_ptr<Tensor> input1 = std::make_shared<Tensor>(Array4D<int,3,3,3,2> {
        {                                       //
            {                                   //
                {{20, 15},{31, 11},{22, 49}},   //
                {{41, 10},{24, 51},{27, 52}},   //
                {{26, 53},{27, 54},{28, 55}}    //
            },                                  //
            {                                   //
                {{29, 56},{30, 57},{31, 58}},   //
                {{32, 59},{33, 60},{34, 61}},   //
                {{35, 62},{36, 63},{37, 64}}    //
            },                                  //
            {                                   //
                {{38, 65},{39, 66},{40, 67}},   //
                {{41, 68},{42, 69},{43, 70}},   //
                {{44, 71},{45, 72},{46, 73}}    //
            }                                   //
        }                                       //
    });                                         //
        std::shared_ptr<Tensor> input2 = std::make_shared<Tensor>(Array4D<int,3,3,3,2> {
            {                                       //
                {                                   //
                    {{20, 47},{21, 48},{22, 49}},   //
                    {{23, 50},{24, 51},{25, 52}},   //
                    {{17, 53},{27, 26},{14, 33}}    //
                },                                  //
                {                                   //
                    {{29, 56},{30, 57},{31, 58}},   //
                    {{72, 44},{33, 20},{27, 55}},   //
                    {{35, 24},{25, 63},{28, 64}}    //
                },                                  //
                {                                   //
                    {{32, 65},{39, 66},{40, 70}},   //
                    {{41, 53},{42, 60},{34, 70}},   //
                    {{44, 71},{30, 12},{46, 73}}    //
                }                                   //
            }                                       //
        });                                         //
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array4D<int,3,3,3,2> {
            {
                {
                    {{1, 0},{0, 0},{1, 1}},
                    {{0, 0},{1, 1},{0, 1}},
                    {{0, 1},{1, 0},{0, 0}}
                },
                {
                    {{1, 1},{1, 1},{1, 1}},
                    {{0, 0},{1, 0},{0, 0}},
                    {{1, 0},{0, 1},{0, 1}}
                },
                {
                    {{0, 1},{1, 1},{1, 0}},
                    {{1, 0},{1, 0},{0, 1}},
                    {{1, 1},{0, 0},{1, 1}}
                }
            }
        });

        std::shared_ptr<Node> myAnd = And();
        auto op = std::static_pointer_cast<OperatorTensor>(myAnd -> getOperator());
        op->associateInput(0, input1);
        op->associateInput(1, input2);
        op->setBackend("cpu");
        op->setDataType(DataType::Int32);
        myAnd->forward();

        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
    }

    SECTION("Broadcasting") {
        std::shared_ptr<Tensor> input_1 = std::make_shared<Tensor>(Array4D<int,1,3,3,2> {
        {                                       //
            {                                   //
                {{10, 20},{22, 23},{20, 20}},   //
                {{10, 15},{10, 29},{20, 20}},   //
                {{26, 25},{33, 20},{10, 20}}    //
            }                                   //
        }                                       //
        });                                     //

        std::shared_ptr<Tensor> input_2 = std::make_shared<Tensor>(Array1D<int,2> {{10, 20}});
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array4D<int,1,3,3,2> {
            {                                   //
                {                               //
                    {{ 1, 1},{ 0, 0},{ 0, 1}},  //
                    {{ 1, 0},{ 1, 0},{ 0, 1}},  //
                    {{ 0, 0},{ 0, 1},{ 1, 1}}   //
                }                               //
            }                                   //
        });                                     //

        std::shared_ptr<Node> myAnd = And();
        auto op = std::static_pointer_cast<OperatorTensor>(myAnd -> getOperator());
        op->associateInput(0, input_1);
        op->associateInput(1, input_2);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myAnd->forward();
        op->getOutput(0)->print();
        expectedOutput->print();
        REQUIRE(*op->getOutput(0) == *expectedOutput);
    }
}