/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <cstdlib>
#include <memory>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/operator/Fold.hpp"
#include "aidge/operator/Unfold.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/operator/Reshape.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] Fold(forward)", "[Fold][CPU]") {
    std::shared_ptr<Node> myUnfold = Unfold({3,3}, "myunfold");
    std::shared_ptr<Node> myReshape = Reshape({4, 27}, "myreshape");
    std::shared_ptr<Node> myMatMul = MatMul("mymatmul");
    std::shared_ptr<Node> myFold = Fold({3,3}, {1,1}, "myfold");
    myUnfold->addChild(myMatMul, 0, 1);
    myReshape->addChild(myMatMul, 0, 0);
    myMatMul->addChild(myFold, 0, 0);

    std::shared_ptr<Tensor> myWeights = std::make_shared<Tensor>(Array4D<int,4,3,3,3> {
        {
            {
                {{  0,   1,   2},
                {  3,   4,   5},
                {  6,   7,   8}},
                {{  9,  10,  11},
                { 12,  13,  14},
                { 15,  16,  17}},
                {{ 18,  19,  20},
                { 21,  22,  23},
                { 24,  25,  26}}
            },
            {
                {{ 27,  28,  29},
                { 30,  31,  32},
                { 33,  34,  35}},
                {{ 36,  37,  38},
                { 39,  40,  41},
                { 42,  43,  44}},
                {{ 45,  46,  47},
                { 48,  49,  50},
                { 51,  52,  53}}
            },
            {
                {{ 54,  55,  56},
                { 57,  58,  59},
                { 60,  61,  62}},
                {{ 63,  64,  65},
                { 66,  67,  68},
                { 69,  70,  71}},
                {{ 72,  73,  74},
                { 75,  76,  77},
                { 78,  79,  80}}
            },
            {
                {{ 81,  82,  83},
                { 84,  85,  86},
                { 87,  88,  89}},
                {{ 90,  91,  92},
                { 93,  94,  95},
                { 96,  97,  98}},
                {{ 99, 100, 101},
                {102, 103, 104},
                {105, 106, 107}}
            }
        }
    });
    std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<int,2,3,5,5> { //NCHW
        {
            {
                {{  0,   1,   2,   3,   4},
                {  5,   6,   7,   8,   9},
                { 10,  11,  12,  13,  14},
                { 15,  16,  17,  18,  19},
                { 20,  21,  22,  23,  24}},

                {{ 25,  26,  27,  28,  29},
                { 30,  31,  32,  33,  34},
                { 35,  36,  37,  38,  39},
                { 40,  41,  42,  43,  44},
                { 45,  46,  47,  48,  49}},

                {{ 50,  51,  52,  53,  54},
                { 55,  56,  57,  58,  59},
                { 60,  61,  62,  63,  64},
                { 65,  66,  67,  68,  69},
                { 70,  71,  72,  73,  74}}
            },
            {
                {{ 75,  76,  77,  78,  79},
                { 80,  81,  82,  83,  84},
                { 85,  86,  87,  88,  89},
                { 90,  91,  92,  93,  94},
                { 95,  96,  97,  98,  99}},

                {{100, 101, 102, 103, 104},
                {105, 106, 107, 108, 109},
                {110, 111, 112, 113, 114},
                {115, 116, 117, 118, 119},
                {120, 121, 122, 123, 124}},

                {{125, 126, 127, 128, 129},
                {130, 131, 132, 133, 134},
                {135, 136, 137, 138, 139},
                {140, 141, 142, 143, 144},
                {145, 146, 147, 148, 149}}
            }
        }
    });
    std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<int,2,4,3,3> {
        {
            {
                {{ 15219, 15570, 15921},
                { 16974, 17325, 17676},
                { 18729, 19080, 19431}},
                {{ 37818, 38898, 39978},
                { 43218, 44298, 45378},
                { 48618, 49698, 50778}},
                {{ 60417, 62226, 64035},
                { 69462, 71271, 73080},
                { 78507, 80316, 82125}},
                {{ 83016, 85554, 88092},
                { 95706, 98244, 100782},
                { 108396, 110934, 113472}}
            },
            {
                {{ 41544, 41895, 42246},
                { 43299, 43650, 44001},
                { 45054, 45405, 45756}},
                {{ 118818, 119898, 120978},
                { 124218, 125298, 126378},
                { 129618, 130698, 131778}},
                {{ 196092, 197901, 199710},
                { 205137, 206946, 208755},
                { 214182, 215991, 217800}},
                {{ 273366, 275904, 278442},
                { 286056, 288594, 291132},
                { 298746, 301284, 303822}}
            }
        }
    });

    auto opUnfold = std::static_pointer_cast<OperatorTensor>(myUnfold -> getOperator());
    auto opReshape = std::static_pointer_cast<OperatorTensor>(myReshape -> getOperator());
    auto opMatMul = std::static_pointer_cast<OperatorTensor>(myMatMul -> getOperator());
    auto opFold = std::static_pointer_cast<OperatorTensor>(myFold -> getOperator());
    opUnfold->associateInput(0,myInput);
    opReshape->associateInput(0,myWeights);

    auto g = getConnectedGraphView(myMatMul);
    g->setDataType(DataType::Int32);
    g->setBackend("cpu");

    g->forwardDims();
    g->save("unfold_matmul_fold");

    SequentialScheduler scheduler(g);
    scheduler.forward();
    //opFold->getOutput(0)->print();
    REQUIRE(*(opFold->getOutput(0)) == *myOutput);
}