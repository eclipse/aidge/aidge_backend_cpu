/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <algorithm>  // std::max, std::min
#include <chrono>
#include <cstddef>  // std::size_t
#include <cstdint>  // std::uint16_t
#include <memory>
#include <random>      // std::random_device, std::mt19937
                       // std::uniform_int_distribution, std::uniform_real_distribution
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <fmt/core.h>

#include "aidge/backend/cpu/operator/ClipImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Clip.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/TensorUtils.hpp"

void ComputeClipBackward(const std::vector<float>& vec1, std::vector<float>& vec2, float min, float max) {
    if (vec1.size() != vec2.size()) {
        fmt::print(stderr, "Vectors should have the same sizes.\n");
        return;
    }

    for (std::size_t i = 0; i < vec1.size(); ++i) {
        if (vec1[i] < min || vec1[i] > max) {
            vec2[i] = 0.0f;
        }
    }
}
namespace Aidge
{
TEST_CASE("[cpu/operator] Clip", "[Clip][CPU]")
 {
    const std::uint16_t NBTRIALS = 10;
    // Create a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(0.0, 10.0);
    std::uniform_real_distribution<float> dismin(0.0, 4.5);
    std::uniform_real_distribution<float> dismax(5.5, 10.0);
    std::uniform_int_distribution<std::size_t> distDims(5,15);
    std::uniform_int_distribution<std::size_t> distNbMatrix(1, 5);

    // Create MatMul Operator
    std::shared_ptr<Node> myClip = Aidge::Clip("nop");
    auto op = std::static_pointer_cast<OperatorTensor>(myClip -> getOperator());

    // To measure execution time of 'MatMul_Op::forward()' member function call
    std::chrono::time_point<std::chrono::system_clock> start;
    std::chrono::time_point<std::chrono::system_clock> end;
    std::chrono::duration<double, std::micro> duration;

    SECTION("Simple clip test [Forward]") {
        std::size_t totalComputation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            // generate Tensors dimensions
            const std::size_t dim0 = distDims(gen);
            const std::size_t dim1 = distDims(gen);
            totalComputation += dim0*dim1;

            // Create and populate the array with random float values
            float* Array = new float[dim0*dim1];
            for (std::size_t i = 0; i < dim0*dim1; ++i) {
                Array[i] = dis(gen); // Generate random float value
            }

            // Convert Input to Tensor
            std::shared_ptr<Tensor> TInput = std::make_shared<Tensor>(DataType::Float32);
            TInput -> resize({dim0,dim1});
            TInput -> setBackend("cpu");
            TInput -> getImpl() -> setRawPtr(Array, dim0*dim1);

            float min = dismin(gen);
            std::shared_ptr<Tensor> Tmin = std::make_shared<Tensor>(DataType::Float32);
            Tmin -> resize({});
            Tmin -> setBackend("cpu");
            Tmin -> getImpl() -> setRawPtr(&min,1);

            float max = dismax(gen);
            std::shared_ptr<Tensor> Tmax = std::make_shared<Tensor>(DataType::Float32);
            Tmax -> resize({});
            Tmax -> setBackend("cpu");
            Tmax -> getImpl() -> setRawPtr(&max,1);
            // convert res to Tensordf
            std::vector<float> GT(Array, Array + (dim0*dim1));
            for (float& val : GT)
            {
                val = std::max(min, std::min(val, max));
            }
            std::shared_ptr<Tensor> Tres = std::make_shared<Tensor>(DataType::Float32);
            Tres -> resize({dim0,dim1});
            Tres -> setBackend("cpu");
            Tres -> getImpl() -> setRawPtr(GT.data(), dim0*dim1);

            op->associateInput(0, TInput);
            op->associateInput(1, Tmin);
            op->associateInput(2, Tmax);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            op->forwardDims(true);

            start = std::chrono::system_clock::now();
            myClip->forward();
            end = std::chrono::system_clock::now();

            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            REQUIRE(approxEq<float>(*(op->getOutput(0)), *Tres));
        }
        Log::info("multiplications over time spent: {}\n", totalComputation/duration.count());
        Log::info("total time: {}\n", duration.count());
    }
    SECTION("Clip test with min >= max [Forward]") {
        std::size_t totalComputation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            // generate Tensors dimensions
            const std::size_t dim0 = distDims(gen);
            const std::size_t dim1 = distDims(gen);
            totalComputation += dim0*dim1;

            // Create and populate the array with random float values
            float* Array = new float[dim0*dim1];
            for (std::size_t i = 0; i < dim0*dim1; ++i) {
                Array[i] = dis(gen); // Generate random float value
            }

            // Convert Input to Tensor
            std::shared_ptr<Tensor> TInput = std::make_shared<Tensor>(DataType::Float32);
            TInput -> resize({dim0,dim1});
            TInput -> setBackend("cpu");
            TInput -> getImpl() -> setRawPtr(Array, dim0*dim1);

            float min = dismax(gen);
            std::shared_ptr<Tensor> Tmin = std::make_shared<Tensor>(DataType::Float32);
            Tmin -> resize({});
            Tmin -> setBackend("cpu");
            Tmin -> getImpl() -> setRawPtr(&min,1);

            float max = dismin(gen); //We generate max and min so that max is always <= min
            std::shared_ptr<Tensor> Tmax = std::make_shared<Tensor>(DataType::Float32);
            Tmax -> resize({});
            Tmax -> setBackend("cpu");
            Tmax -> getImpl() -> setRawPtr(&max,1);
            // convert res to Tensor
            std::vector<float> GT(Array, Array + (dim0*dim1));
            for (float& val : GT)
            {
                val = max;
            }
            std::shared_ptr<Tensor> Tres = std::make_shared<Tensor>(DataType::Float32);
            Tres -> resize({dim0,dim1});
            Tres -> setBackend("cpu");
            Tres -> getImpl() -> setRawPtr(GT.data(), dim0*dim1);

            op->associateInput(0, TInput);
            op->associateInput(1, Tmin);
            op->associateInput(2, Tmax);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            op->forwardDims(true);

            start = std::chrono::system_clock::now();
            myClip->forward();
            end = std::chrono::system_clock::now();

            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            REQUIRE(approxEq<float>(*(op->getOutput(0)), *Tres));
        }
        Log::info("multiplications over time spent: {}\n", totalComputation/duration.count());
        Log::info("total time: {}\n", duration.count());
    }
    SECTION("Clip with Clip Attr [Forward]")
    {
        std::size_t totalComputation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial)
        {

            float min = dismin(gen);
            float max = dismax(gen);
            std::shared_ptr<Node> myCl = Aidge::Clip("",min,max);
            auto op = std::static_pointer_cast<OperatorTensor>(myCl -> getOperator());


            // generate Tensors dimensions
            const std::size_t dim0 = 3;
            const std::size_t dim1 = 3;
            totalComputation += dim0*dim1;

            // Create and populate the array with random float values
            float* Array = new float[dim0*dim1];
            for (std::size_t i = 0; i < dim0*dim1; ++i) {
                Array[i] = dis(gen); // Generate random float value
            }
            // Convert Input to Tensor
            std::shared_ptr<Tensor> TInput = std::make_shared<Tensor>(DataType::Float32);
            TInput -> resize({dim0,dim1});
            TInput -> setBackend("cpu");
            TInput -> getImpl() -> setRawPtr(Array, dim0*dim1);

            // convert res to Tensordf
            std::vector<float> GT(Array, Array + (dim0*dim1));
            for (float& val : GT)
            {
                val = std::max(min, std::min(val, max));
            }
            std::shared_ptr<Tensor> Tres = std::make_shared<Tensor>(DataType::Float32);
            Tres -> resize({dim0,dim1});
            Tres -> setBackend("cpu");
            Tres -> getImpl() -> setRawPtr(GT.data(), dim0*dim1);
            op->associateInput(0, TInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            op->forwardDims(true);
            start = std::chrono::system_clock::now();
            myCl->forward();
            end = std::chrono::system_clock::now();

            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            REQUIRE(approxEq<float>(*(op->getOutput(0)), *Tres));
        }
        Log::info("multiplications over time spent: {}\n", totalComputation/duration.count());
        Log::info("total time: {}\n", duration.count());
    }
    SECTION("Simple clip test [Backward]") {
        std::size_t totalComputation = 0;
        duration = std::chrono::duration<double, std::micro>::zero();
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            std::size_t totalComputation = 0;
        for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
            // generate Tensors dimensions
            const std::size_t dim0 = distDims(gen);
            const std::size_t dim1 = distDims(gen);

            totalComputation += dim0*dim1;

            // Create and populate the array with random float values
            float* Array = new float[dim0*dim1];
            float* gradArray = new float[dim0*dim1];
            for (std::size_t i = 0; i < dim0*dim1; ++i) {
                Array[i] = dis(gen); // Generate random float value
                gradArray[i] = dis(gen);
            }

            std::shared_ptr<Tensor> TGrad = std::make_shared<Tensor>(DataType::Float32);
            TGrad -> resize({dim0,dim1});
            TGrad -> setBackend("cpu");
            TGrad -> getImpl() -> setRawPtr(gradArray, dim0*dim1);

            // Convert Input to Tensor
            std::shared_ptr<Tensor> TInput = std::make_shared<Tensor>(DataType::Float32);
            TInput -> resize({dim0,dim1});
            TInput -> setBackend("cpu");
            TInput -> getImpl() -> setRawPtr(Array, dim0*dim1);

            float min = dismin(gen);
            std::shared_ptr<Tensor> Tmin = std::make_shared<Tensor>(DataType::Float32);
            Tmin -> resize({});
            Tmin -> setBackend("cpu");
            Tmin -> getImpl() -> setRawPtr(&min,1);

            float max = dismax(gen);
            std::shared_ptr<Tensor> Tmax = std::make_shared<Tensor>(DataType::Float32);
            Tmax -> resize({});
            Tmax -> setBackend("cpu");
            Tmax -> getImpl() -> setRawPtr(&max,1);
            // convert res to Tensor
            std::vector<float> GT(Array, Array + (dim0*dim1));
            for (float& val : GT)
            {
                val = std::max(min, std::min(val, max));//Clip operation
            }
            std::shared_ptr<Tensor> Tres = std::make_shared<Tensor>(DataType::Float32);
            Tres -> resize({dim0,dim1});
            Tres -> setBackend("cpu");
            Tres -> getImpl() -> setRawPtr(GT.data(), dim0*dim1);

            op->associateInput(0, TInput);
            op->associateInput(1, Tmin);
            op->associateInput(2, Tmax);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            op->forwardDims(true);
            myClip->forward();

            op->getOutput(0)->setGrad(TGrad);

            start = std::chrono::system_clock::now();
            REQUIRE_NOTHROW(myClip->backward());
            end = std::chrono::system_clock::now();

            auto GradTensor = op->getInput(0)->grad();
            float* BackwardTensor = (float*)GradTensor->getImpl()->rawPtr();
            std::vector<float> GT0(Array,Array+(dim0*dim1));
            std::vector<float> GT1(gradArray,gradArray+(dim0*dim1));
            std::vector<float> BackwardTensorVec(BackwardTensor,BackwardTensor+(dim0*dim1));
            ComputeClipBackward(GT0,GT1,min,max);
            duration += std::chrono::duration_cast<std::chrono::microseconds>(end - start);
            REQUIRE(GT1 == BackwardTensorVec);
        }
        Log::info("multiplications over time spent: {}\n", totalComputation/duration.count());
        Log::info("total time: {}\n", duration.count());
    }
 }
} // namespace Aidge
}