/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>

#include <catch2/catch_test_macros.hpp>

#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/backend/cpu/operator/ReLUImpl.hpp"
#include "aidge/data/DataType.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/ReLU.hpp"


using namespace Aidge;

TEST_CASE("[cpu/operator] ReLU(forward)", "[ReLU][CPU]") {
    SECTION("1D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array1D<int,10> {
            {0, 1, 2,-3, 4,-5,-6, 7, 8, 9}
        });
        Tensor expectedOutput = Array1D<int,10> {
            {0, 1, 2, 0, 4, 0, 0, 7, 8, 9}
        };

        std::shared_ptr<ReLU_Op> op = std::make_shared<ReLU_Op>();
        op->associateInput(0,input0);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        op->forward();
        REQUIRE(*(op->getOutput(0)) == expectedOutput);
    }

    SECTION("2D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array2D<int,2,10> {
            {
                { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
            }
        });
        Tensor expectedOutput = Array2D<int,2,10> {
            {
                { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
            }
        };

        std::shared_ptr<ReLU_Op> op = std::make_shared<ReLU_Op>();
        op->associateInput(0,input0);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        op->forward();
        REQUIRE(*op->getOutput(0) == expectedOutput);
    }

    SECTION("3D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array3D<int,2,2,10> {
            {
                {
                    { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                    {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                },
                {
                    { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                    {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                }
            }
        });
        Tensor expectedOutput = Array3D<int,2,2,10> {
            {
                {
                    { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                    { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                },
                {
                    { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                    { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                }
            }
        };

        std::shared_ptr<ReLU_Op> op = std::make_shared<ReLU_Op>();
        op->associateInput(0,input0);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        op->forward();
        REQUIRE(*(op->getOutput(0)) == expectedOutput);
    }

    SECTION("4D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array4D<int,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                },
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                }
            }
        });
        Tensor expectedOutput = Array4D<int,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    },
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    }
                },
                {
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    },
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    }
                }
            }
        };

        std::shared_ptr<ReLU_Op> op = std::make_shared<ReLU_Op>();
        op->associateInput(0,input0);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        op->forward();
        REQUIRE(*op->getOutput(0) == expectedOutput);
    }
}