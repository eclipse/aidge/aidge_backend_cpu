/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Slice.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] Slice(forward)", "[Slice][CPU]") {
    SECTION("1D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array1D<int,10> {
            {0, 1, -2,-3, 4,-5,-6, 7, 8, 9}
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array1D<int,3> {
            {0, 1, -2}
        });
        std::shared_ptr<Tensor> starts = std::make_shared<Tensor>(Array1D<int,1>{{0}});
        std::shared_ptr<Tensor> ends = std::make_shared<Tensor>(Array1D<int,1>{{3}});
        std::shared_ptr<Tensor> axes = std::make_shared<Tensor>(Array1D<int,1>{{0}});

        std::shared_ptr<Node> mySlice = Slice();
        auto op = std::static_pointer_cast<OperatorTensor>(mySlice -> getOperator());
        mySlice->getOperator()->associateInput(0,input0);
        mySlice->getOperator()->associateInput(1,starts);
        mySlice->getOperator()->associateInput(2,ends);
        mySlice->getOperator()->associateInput(3,axes);
        mySlice->getOperator()->setDataType(DataType::Int32);
        mySlice->getOperator()->setBackend("cpu");
        mySlice->forward();

        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
        REQUIRE(op->getOutput(0)->dims() == expectedOutput->dims());
        REQUIRE(op->getOutput(0)->dataType() == expectedOutput->dataType());
    }

    SECTION("2D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array2D<int,2,10> {
            {
                { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array2D<int,2,3> {
            {
                {-5,-6, 7},
                {-5,-6, 7}
            }
        });
        std::shared_ptr<Tensor> starts = std::make_shared<Tensor>(Array1D<int,2>{{0,5}});
        std::shared_ptr<Tensor> ends = std::make_shared<Tensor>(Array1D<int,2>{{2,8}});
        std::shared_ptr<Tensor> axes = std::make_shared<Tensor>(Array1D<int,2>{{0,1}});

        std::shared_ptr<Node> mySlice = Slice();
        auto op = std::static_pointer_cast<OperatorTensor>(mySlice -> getOperator());
        mySlice->getOperator()->associateInput(0,input0);
        mySlice->getOperator()->associateInput(1,starts);
        mySlice->getOperator()->associateInput(2,ends);
        mySlice->getOperator()->associateInput(3,axes);
        mySlice->getOperator()->setDataType(DataType::Int32);
        mySlice->getOperator()->setBackend("cpu");
        mySlice->forward();
        // op->getOutput(0)->print();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
        REQUIRE(op->getOutput(0)->dims() == expectedOutput->dims());
        REQUIRE(op->getOutput(0)->dataType() == expectedOutput->dataType());
    }

    SECTION("3D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array3D<int,2,2,10> {
            {
                {
                    { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                    {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                },
                {
                    { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                    {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array3D<int,1,1,3> {
            {
                {
                    { 4,-5,-6}
                }
            }
        });
        std::shared_ptr<Tensor> starts = std::make_shared<Tensor>(Array1D<int,3>{{0,1,4}});
        std::shared_ptr<Tensor> ends = std::make_shared<Tensor>(Array1D<int,3>{{1,2,7}});
        std::shared_ptr<Tensor> axes = std::make_shared<Tensor>(Array1D<int,3>{{0,1,2}});

        std::shared_ptr<Node> mySlice = Slice();
        auto op = std::static_pointer_cast<OperatorTensor>(mySlice -> getOperator());
        mySlice->getOperator()->associateInput(0,input0);
        mySlice->getOperator()->associateInput(1,starts);
        mySlice->getOperator()->associateInput(2,ends);
        mySlice->getOperator()->associateInput(3,axes);
        mySlice->getOperator()->setDataType(DataType::Int32);
        mySlice->getOperator()->setBackend("cpu");
        mySlice->forward();
        // mySlice->getOperator()->output(0).print();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
        REQUIRE(op->getOutput(0)->dims() == expectedOutput->dims());
        REQUIRE(op->getOutput(0)->dataType() == expectedOutput->dataType());
    }

    SECTION("4D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array4D<int,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                },
                {
                    {
                        { 0, 1, 2,-3, 6,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3,11,-5,-6, 7,-1,10}
                    }
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array4D<int,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                },
                {
                    {
                        { 0, 1, 2,-3, 6,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3,11,-5,-6, 7,-1,10}
                    }
                }
            }
        });
        std::shared_ptr<Tensor> starts = std::make_shared<Tensor>(Array1D<int,4>{{0,0,0,0}});
        std::shared_ptr<Tensor> ends = std::make_shared<Tensor>(Array1D<int,4>{{2,2,2,10}});
        std::shared_ptr<Tensor> axes = std::make_shared<Tensor>(Array1D<int,4>{{0,1,2,3}});

        std::shared_ptr<Node> mySlice = Slice();
        auto op = std::static_pointer_cast<OperatorTensor>(mySlice -> getOperator());
        mySlice->getOperator()->associateInput(0,input0);
        mySlice->getOperator()->associateInput(1,starts);
        mySlice->getOperator()->associateInput(2,ends);
        mySlice->getOperator()->associateInput(3,axes);
        mySlice->getOperator()->setDataType(DataType::Int32);
        mySlice->getOperator()->setBackend("cpu");
        mySlice->forward();
        // op->getOutput(0)->print();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
        REQUIRE(op->getOutput(0)->dims() == expectedOutput->dims());
        REQUIRE(op->getOutput(0)->dataType() == expectedOutput->dataType());
    }

    SECTION("Attributes instead of inputs") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array4D<int,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                },
                {
                    {
                        { 0, 1, 2,-3, 6,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3,11,-5,-6, 7,-1,10}
                    }
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array4D<int,1,1,1,5> {
            {
                {
                    {
                        { 0, 1, 2,-3, 4}
                    }
                }
            }
        });

        std::shared_ptr<Node> mySlice = Slice({0,0,0,0}, {1,1,1,5}, {0,1,2,3}, {1,1,1,1});
        auto op = std::static_pointer_cast<OperatorTensor>(mySlice -> getOperator());
        mySlice->getOperator()->associateInput(0,input0);
        mySlice->getOperator()->setDataType(DataType::Int32);
        mySlice->getOperator()->setBackend("cpu");
        mySlice->forward();
        // op->getOutput(0)->print();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
        REQUIRE(op->getOutput(0)->dims() == expectedOutput->dims());
        REQUIRE(op->getOutput(0)->dataType() == expectedOutput->dataType());
    }

    SECTION("Different Steps") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array3D<int,4,2,8> {
            {
                {
                    { 0, 1, 2,-3, 4,-5,-6,7},
                    {-5, 4, 2,-3, 4,-5,-6,-7}
                },
                {
                    { 10, 11, 12,-13, 14,-15,-16,17},
                    {-15, 14, 12,-13, 14,-15,-16,-17}
                },
                {
                    { 20, 21, 22,-23, 24,-25,-26,27},
                    {-25, 24, 22,-23, 24,-25,-26,-27}
                },
                {
                    { 30, 31, 32,-33, 34,-35,-36,37},
                    {-35, 34, 32,-33, 34,-35,-36,-37}
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array3D<int,2,1,3> {
            {
                {
                    { 7, 4, 1}
                },
                {
                    { 27, 24, 21}
                }
            }
        });

        std::shared_ptr<Node> mySlice = Slice({0,0,7}, {4,1,0}, {0,1,2}, {2,1,-3});
        // Steps are 2,1,-3 so the slice will be:
        // on Axis 0: from 0 to 4 by step of 2
        // on Axis 1: from 0 to 1 by step of 1
        // on Axis 2: from 7 to 0 by step of -3 (reverse the order of elements)
        auto op = std::static_pointer_cast<OperatorTensor>(mySlice -> getOperator());
        mySlice->getOperator()->associateInput(0,input0);
        mySlice->getOperator()->setDataType(DataType::Int32);
        mySlice->getOperator()->setBackend("cpu");
        mySlice->forward();
        op->getOutput(0)->print();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
        REQUIRE(op->getOutput(0)->dims() == expectedOutput->dims());
        REQUIRE(op->getOutput(0)->dataType() == expectedOutput->dataType());
    }
}
