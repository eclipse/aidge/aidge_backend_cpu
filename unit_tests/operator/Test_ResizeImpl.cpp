/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstdint>
#include <memory>

#include <aidge/data/Data.hpp>
#include <aidge/data/Interpolation.hpp>
#include <aidge/data/half.hpp>
#include <aidge/operator/Pad.hpp>
#include <aidge/utils/ArrayHelpers.hpp>
#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Resize.hpp"
#include "aidge/utils/TensorUtils.hpp"

namespace Aidge {

TEST_CASE("[cpu/operator] Resize(forward)", "[Resize][CPU]") {

    Log::setConsoleLevel(Log::Level::Debug);

    SECTION("Nearest") {
        SECTION("Ceil") {
            std::shared_ptr<Tensor> input_tensor = std::make_shared<Tensor>(Array4D<std::int32_t, 1, 1, 2, 2>{{
                {
                    {
                        { 1, 2},
                        { 3, 4}
                    }
                }
            }});
            Tensor expected_out_tensor = Tensor(Array4D<std::int32_t, 1, 1, 4, 4>{{
                {
                    {
                        { 1, 1, 1, 2},
                        { 1, 1, 1, 2},
                        { 1, 1, 1, 2},
                        { 3, 3, 3, 4}
                    }
                }
            }});

            std::vector<float> scales = {1.0f, 1.0f, 2.0f, 2.0f};
            auto resize_node = Resize(scales, {}, Interpolation::CoordinateTransformation::HalfPixel, Interpolation::Mode::Floor);
            auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
            op->associateInput(0, input_tensor);


            op->setDataType(DataType::Int32);
            op->setBackend("cpu");
            op->forwardDims(true);
            op->forward();

            op->getOutput(0)->print();
            expected_out_tensor.print();

            CHECK(*(op->getOutput(0)) == expected_out_tensor);
        }
    }

    SECTION("1-sized input tensor (upscaling)") {
        std::shared_ptr<Tensor> input_tensor = std::make_shared<Tensor>(Array4D<float, 1, 1, 1, 1>{{{{{0.417022}}}}});

        std::vector<std::size_t> sizes = {1, 1, 2, 2};
        auto resize_node = Resize({}, sizes, Interpolation::CoordinateTransformation::HalfPixel, Interpolation::Mode::Linear);
        auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
        op->associateInput(0, input_tensor);


        op->setDataType(DataType::Float32);
        op->setBackend("cpu");
        op->forwardDims(true);
        op->forward();
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array4D<float, 1, 1, 2, 2>{
            {{{{0.417022, 0.417022}, {0.417022, 0.417022}}}}});
        op->getOutput(0)->print();
        CHECK(approxEq<float>(*op->getOutput(0), *expectedOutput) == true);
    }
    SECTION("Upscaling from 5x5 to 10x10 (linear)") {
        std::shared_ptr<Tensor> input_tensor = std::make_shared<Tensor>(
            Array4D<float, 1, 1, 5, 5>{{{{{7.20324516e-01,
                                               1.14374816e-04,
                                               3.02332580e-01,
                                               1.46755889e-01,
                                               9.23385918e-02},
                                              {1.86260208e-01,
                                               3.45560730e-01,
                                               3.96767467e-01,
                                               5.38816750e-01,
                                               4.19194520e-01},
                                              {6.85219526e-01,
                                               2.04452246e-01,
                                               8.78117442e-01,
                                               2.73875929e-02,
                                               6.70467496e-01},
                                              {4.17304814e-01,
                                               5.58689833e-01,
                                               1.40386939e-01,
                                               1.98101491e-01,
                                               8.00744593e-01},
                                              {9.68261600e-01,
                                               3.13424170e-01,
                                               6.92322612e-01,
                                               8.76389146e-01,
                                               8.94606650e-01}}}}}
        );

        std::vector<std::size_t> sizes = {1, 1, 10, 10};
        auto resize_node = Resize({}, sizes, Interpolation::CoordinateTransformation::Asymmetric, Interpolation::Mode::Linear);
        auto op = std::static_pointer_cast<Resize_Op>(resize_node->getOperator());
        op->associateInput(0, input_tensor);

        op->setDataType(DataType::Float32);
        op->setBackend("cpu");
        op->forwardDims(true);
        op->forward();
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(
            Array4D<float, 1, 1, 10, 10>{{{{{7.20324516e-01,
                                             3.60219449e-01,
                                             1.14374816e-04,
                                             1.51223481e-01,
                                             3.02332580e-01,
                                             2.24544227e-01,
                                             1.46755889e-01,
                                             1.19547240e-01,
                                             9.23385918e-02,
                                             9.23385918e-02},

                                            {4.53292370e-01,
                                             3.13064963e-01,
                                             1.72837555e-01,
                                             2.61193782e-01,
                                             3.49550009e-01,
                                             3.46168160e-01,
                                             3.42786312e-01,
                                             2.99276441e-01,
                                             2.55766571e-01,
                                             2.55766571e-01},

                                            {1.86260208e-01,
                                             2.65910476e-01,
                                             3.45560730e-01,
                                             3.71164083e-01,
                                             3.96767467e-01,
                                             4.67792094e-01,
                                             5.38816750e-01,
                                             4.79005635e-01,
                                             4.19194520e-01,
                                             4.19194520e-01},

                                            {4.35739875e-01,
                                             3.55373204e-01,
                                             2.75006473e-01,
                                             4.56224471e-01,
                                             6.37442470e-01,
                                             4.60272312e-01,
                                             2.83102185e-01,
                                             4.13966596e-01,
                                             5.44831038e-01,
                                             5.44831038e-01},

                                            {6.85219526e-01,
                                             4.44835901e-01,
                                             2.04452246e-01,
                                             5.41284859e-01,
                                             8.78117442e-01,
                                             4.52752531e-01,
                                             2.73875929e-02,
                                             3.48927557e-01,
                                             6.70467496e-01,
                                             6.70467496e-01},

                                            {5.51262140e-01,
                                             4.66416597e-01,
                                             3.81571054e-01,
                                             4.45411623e-01,
                                             5.09252191e-01,
                                             3.10998380e-01,
                                             1.12744540e-01,
                                             4.24175322e-01,
                                             7.35606015e-01,
                                             7.35606015e-01},

                                            {4.17304814e-01,
                                             4.87997323e-01,
                                             5.58689833e-01,
                                             3.49538386e-01,
                                             1.40386939e-01,
                                             1.69244215e-01,
                                             1.98101491e-01,
                                             4.99423027e-01,
                                             8.00744593e-01,
                                             8.00744593e-01},

                                            {6.92783237e-01,
                                             5.64420104e-01,
                                             4.36057001e-01,
                                             4.26205903e-01,
                                             4.16354775e-01,
                                             4.76800054e-01,
                                             5.37245333e-01,
                                             6.92460477e-01,
                                             8.47675622e-01,
                                             8.47675622e-01},

                                            {9.68261600e-01,
                                             6.40842915e-01,
                                             3.13424170e-01,
                                             5.02873421e-01,
                                             6.92322612e-01,
                                             7.84355879e-01,
                                             8.76389146e-01,
                                             8.85497928e-01,
                                             8.94606650e-01,
                                             8.94606650e-01},

                                            {9.68261600e-01,
                                             6.40842915e-01,
                                             3.13424170e-01,
                                             5.02873421e-01,
                                             6.92322612e-01,
                                             7.84355879e-01,
                                             8.76389146e-01,
                                             8.85497928e-01,
                                             8.94606650e-01,
                                             8.94606650e-01}}}}});
        Log::notice("Expected result : dims = {}", expectedOutput->dims());
        expectedOutput->print();
        Log::notice("\nActual result: dims = {}", op->getOutput(0)->dims());
        op->getOutput(0)->print();
        CHECK(approxEq<float>(*op->getOutput(0),
                              *expectedOutput,
                              1e-5f,
                              1e-5f) == true);
    }
}

} // namespace Aidge
