/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cmath>    // std::abs
#include <cstddef>  // std::size_t
#include <memory>

#include <catch2/catch_test_macros.hpp>

#include "aidge/backend/cpu/operator/BatchNormImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/BatchNorm.hpp"
#include "aidge/utils/ArrayHelpers.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] BatchNorm(forward)", "[BatchNorm][CPU]") {
    std::shared_ptr<Node> myBatchNorm = BatchNorm<2>(3, 0.00001F, 0.1F, "mybatchnorm");
    auto op = std::static_pointer_cast<BatchNorm_Op<2>>(myBatchNorm -> getOperator());
    std::shared_ptr<Tensor> myWeights = std::make_shared<Tensor>(Array1D<float,3> {{0.9044, 0.3028, 0.0218}});
    std::shared_ptr<Tensor> myBias = std::make_shared<Tensor>(Array1D<float,3> {{0.1332, 0.7503, 0.0878}});
    std::shared_ptr<Tensor> myMean = std::make_shared<Tensor>(Array1D<float,3> {{0.9931, 0.8421, 0.9936}});
    std::shared_ptr<Tensor> myVar = std::make_shared<Tensor>(Array1D<float,3> {{0.4470, 0.3064, 0.7061}});
    std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<float,2,3,3,3> { //NCHW
        {
                {
                    {{8.28257084e-01, 7.99335480e-01, 7.36702740e-01},
                     {2.36729562e-01, 8.61912668e-01, 9.93067741e-01},
                     {1.63514376e-01, 8.95773172e-02, 2.96533108e-01}},
                    {{2.20776618e-01, 5.89067876e-01, 2.03930080e-01},
                     {1.31294072e-01, 7.10182846e-01, 1.08420849e-04},
                     {7.21750259e-01, 4.38212037e-01, 5.08823872e-01}},
                    {{4.30953979e-01, 1.51903450e-01, 3.76343548e-01},
                     {8.07861805e-01, 7.79679358e-01, 5.01209974e-01},
                     {9.31280375e-01, 9.94207084e-01, 1.74868107e-03}}
                },
                {
                    {{6.22058094e-01, 2.32256651e-02, 6.18222237e-01},
                     {9.58304763e-01, 2.11395025e-02, 4.95614648e-01},
                     {2.50825584e-01, 4.50860739e-01, 3.80362332e-01}},
                    {{9.91703272e-02, 5.06073236e-01, 4.88969564e-01},
                     {1.12059772e-01, 7.64178872e-01, 7.60362148e-01},
                     {2.84135342e-02, 4.29610193e-01, 1.27862811e-01}},
                    {{9.57209170e-01, 8.22797656e-01, 1.91352129e-01},
                     {9.52722490e-01, 6.35501027e-01, 5.67592978e-02},
                     {2.00799644e-01, 4.00822222e-01, 9.14380193e-01}}
                }
            }
    });
    std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float,2,3,3,3> {
        {
            {
                {{-0.08978321, -0.12890550, -0.21362889},
                 {-0.88994324, -0.04425725,  0.13315639},
                 {-0.98898154, -1.08899629, -0.80904692}},
                {{ 0.41042271,  0.61188596,  0.40120730},
                 { 0.36147383,  0.67813843,  0.28971246},
                 { 0.68446606,  0.52936459,  0.56799078}},
                {{ 0.07320327,  0.06596386,  0.07178652},
                 { 0.08298140,  0.08225026,  0.07502592},
                 { 0.08618324,  0.08781575,  0.06206840}}
            },
            {
                {{-0.36870885, -1.17875028, -0.37389761},
                 { 0.08613246, -1.18157220, -0.53974909},
                 {-0.87087554, -0.60028774, -0.69565099}},
                {{ 0.34390146,  0.56648612,  0.55713004},
                 { 0.35095227,  0.70767546,  0.70558763},
                 { 0.30519596,  0.52465916,  0.35959685}},
                {{ 0.08685592,  0.08336888,  0.06698728},
                 { 0.08673952,  0.07850984,  0.06349554},
                 { 0.06723238,  0.07242157,  0.08574481}}
            }
        }
    });
    op->associateInput(0,myInput);
    op->associateInput(1,myWeights);
    op->associateInput(2,myBias);
    op->associateInput(3,myMean);
    op->associateInput(4,myVar);
    op->setDataType(DataType::Float32);
    op->setBackend("cpu");
    myBatchNorm->forward();

    float* resPtr = static_cast<float*>(op->getOutput(0)->getImpl()->rawPtr());
    float* expectedPtr = static_cast<float*>(myOutput->getImpl()->rawPtr());
    for (std::size_t i = 0; i< 54; ++i) {
        REQUIRE(std::abs(resPtr[i]-expectedPtr[i]) < 0.00001);
    }

    // std::cout << static_cast<Tensor>((*op)["weight"])[0][0][0][0] << std::endl;
}