/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cmath>    // std::abs
#include <cstddef>  // std::size_t
#include <memory>

#include <catch2/catch_test_macros.hpp>

#include "aidge/backend/cpu/operator/AtanImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/Atan.hpp"
#include "aidge/utils/ArrayHelpers.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] Atan(forward)") {
  SECTION("1D Tensor") {
    std::shared_ptr<Tensor> input0 =
        std::make_shared<Tensor>(Array1D<float, 10>{
            {0.41384590, 0.43120754, 0.93762982, 0.31049860, 0.77547199,
             0.09514862, 0.16145366, 0.42776686, 0.43487436, 0.41170865}});
    std::shared_ptr<Tensor> expectedOutput =
        std::make_shared<Tensor>(Array1D<float, 10>{
            {0.39238522, 0.40711672, 0.75322037, 0.30106049, 0.65960488,
             0.09486303, 0.16007232, 0.40421187, 0.4102045, 0.39055911}});

    std::shared_ptr<Node> myAtan = Atan();
    auto op = std::static_pointer_cast<Atan_Op>(myAtan->getOperator());
    op->associateInput(0, input0);
    op->setDataType(DataType::Float32);
    op->setBackend("cpu");
    myAtan->forward();

    float* resPtr = static_cast<float*>(op->getOutput(0)->getImpl()->rawPtr());
    float* expectedPtr =
        static_cast<float*>(expectedOutput->getImpl()->rawPtr());
    for (std::size_t i = 0; i < expectedOutput->size(); ++i) {
      REQUIRE(std::abs(resPtr[i] - expectedPtr[i]) < 0.00001);
    }
  }

  SECTION("3D Tensor") {
    std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(
        Array3D<float, 2, 2, 3>{{{
                                     {0.97037154, 0.86208081, 0.77767169},
                                     {0.38160080, 0.11422747, 0.77284443},
                                 },
                                 {{0.51592529, 0.72543722, 0.54641193},
                                  {0.93866944, 0.97767913, 0.34172094}}}});
    std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(
        Array3D<float, 2, 2, 3>{{{{0.77036231, 0.71146592, 0.66097706},
                                  {0.36454508, 0.11373451, 0.65796196}},
                                 {{0.47630652, 0.62759472, 0.50008428},
                                  {0.75377332, 0.77411225, 0.32928031}}}});

    std::shared_ptr<Node> myAtan = Atan();
    auto op = std::static_pointer_cast<Atan_Op>(myAtan->getOperator());
    op->associateInput(0, input0);
    op->setDataType(DataType::Float32);
    op->setBackend("cpu");
    myAtan->forward();

    float* resPtr = static_cast<float*>(op->getOutput(0)->getImpl()->rawPtr());
    float* expectedPtr =
        static_cast<float*>(expectedOutput->getImpl()->rawPtr());
    for (std::size_t i = 0; i < expectedOutput->size(); ++i) {
      REQUIRE(std::abs(resPtr[i] - expectedPtr[i]) < 0.00001);
    }
  }
}
