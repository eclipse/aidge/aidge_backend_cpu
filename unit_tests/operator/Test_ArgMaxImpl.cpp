/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstddef> // std::size_t
#include <cstdint> // std::uint16_t
#include <memory>
#include <random>  // std::random_device, std::mt19937, std::uniform_int_distribution, std::uniform_real_distribution

#include <catch2/catch_test_macros.hpp>
#include <fmt/core.h>

#include "aidge/backend/cpu/operator/ArgMaxImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/ArgMax.hpp"
#include "aidge/utils/ArrayHelpers.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] ArgMax(forward)", "[ArgMax][CPU]") {
    SECTION("ForwardDims")
    {
        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(0.1f, 1.1f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(2), std::size_t(10));
        std::uniform_int_distribution<std::size_t> nbDimsDist(std::size_t(1), std::size_t(5));
        std::uniform_int_distribution<int> boolDist(0,1);

        SECTION("KeepDims") {
            for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
                DimSize_t nbDims = nbDimsDist(gen);
                std::vector<DimSize_t> dims(nbDims);
                std::vector<DimSize_t> expectedOutDims(nbDims);
                std::uniform_int_distribution<std::int32_t> axisDist(std::int32_t(0), std::int32_t(nbDims-1));
                std::int32_t axis = axisDist(gen);
                for (std::size_t i = 0; i < nbDims; i++) {
                    dims[i] = dimSizeDist(gen);
                    if (i == axis) {
                        expectedOutDims[i] = 1;
                    }
                    else {
                        expectedOutDims[i] = dims[i];
                    }
                }

                std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(dims);
                myInput->setBackend("cpu");
                myInput->setDataType(DataType::Float32);
                myInput->zeros();
                std::shared_ptr<Node> myArgMax = ArgMax(axis);
                auto op = std::static_pointer_cast<OperatorTensor>(myArgMax -> getOperator());
                op->associateInput(0,myInput);
                op->setDataType(DataType::Float32);
                op->setBackend("cpu");
                op->forwardDims();

                const auto outputDims = op->getOutput(0)->dims();
                REQUIRE(outputDims == expectedOutDims);
            }
        }
        SECTION("Not KeepDims") {
            for (std::uint16_t trial = 0; trial < NBTRIALS; ++trial) {
                DimSize_t nbDims = nbDimsDist(gen);
                std::vector<DimSize_t> dims(nbDims);
                std::vector<DimSize_t> expectedOutDims;
                std::uniform_int_distribution<std::int32_t> axisDist(std::int32_t(0), std::int32_t(nbDims-1));
                std::int32_t axis = axisDist(gen);
                for (std::size_t i = 0; i < nbDims; i++) {
                    dims[i] = dimSizeDist(gen);
                    if(i != axis) {
                        expectedOutDims.push_back(dims[i]);
                    }
                }
                if(expectedOutDims.empty()) {
                    expectedOutDims.push_back(1);
                }

                std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(dims);
                myInput->setBackend("cpu");
                myInput->setDataType(DataType::Float32);
                std::shared_ptr<Node> myArgMax = ArgMax(axis, false);
                auto op = std::static_pointer_cast<OperatorTensor>(myArgMax -> getOperator());
                op->associateInput(0,myInput);
                op->setDataType(DataType::Float32);
                op->setBackend("cpu");

                op->forwardDims();

                const auto outputDims = op->getOutput(0)->dims();
                REQUIRE(outputDims == expectedOutDims);
            }
        }
    }
    SECTION("3D Tensor") {
            std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array3D<float,2,3,4> {
                {
                    {
                        { 1.0, 2.0, 3.0, 4.0},
                        { 8.0, 0.0, 17.0, 1.0},
                        { 5.0, 10.0, 6.0, 0.0}
                    },
                    {
                        { 7.0, 1.0, 9.0, 4.0},
                        { 0.0, 8.0, 4.0, 2.0},
                        { 9.0, 2.0, 0.0, 5.0}
                    }
                }
            });
        SECTION("Axis 2") {

            Tensor myOutput = Tensor(Array3D<float,2,3, 1> {
               {
                    {
                        {3.0},
                        {2.0},
                        {1.0}
                    },
                    {
                        {2.0},
                        {1.0},
                        {0.0}
                    }
               }
            });

            std::shared_ptr<Node> myArgMax = ArgMax(2);
            auto op = std::static_pointer_cast<OperatorTensor>(myArgMax -> getOperator());
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            myArgMax->forward();

            REQUIRE(*(op->getOutput(0)) == myOutput);
        }
        SECTION("Axis 2 with keep_dims false") {

            Tensor myOutput = Tensor(Array2D<float,2,3> {
               {
                    { 3.0, 2.0, 1.0 },
                    { 2.0, 1.0, 0.0 }
               }
            });

            std::shared_ptr<Node> myArgMax = ArgMax(2,0);
            auto op = std::static_pointer_cast<OperatorTensor>(myArgMax -> getOperator());
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            myArgMax->forward();

            REQUIRE(*(op->getOutput(0)) == myOutput);
        }
        SECTION("Axis 1") {
            Tensor myOutput = Tensor(Array3D<float,2,1,4> {
                {
                    {
                        { 1.0, 2.0, 1.0, 0.0 }
                    },
                    {
                        { 2.0, 1.0, 0.0, 2.0 }
                    }
                }
            });

            std::shared_ptr<Node> myArgMax = ArgMax(1);
            auto op = std::static_pointer_cast<OperatorTensor>(myArgMax -> getOperator());
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            myArgMax->forward();

            REQUIRE(*(op->getOutput(0)) == myOutput);
        }
        SECTION("Axis 0") {
            Tensor myOutput = Tensor(Array3D<float,1,3,4> {
                {
                    {
                        { 1.0, 0.0, 1.0, 0.0 },
                        { 0.0, 1.0, 0.0, 1.0 },
                        { 1.0, 0.0, 0.0, 1.0 }
                    }
                }
            });

            std::shared_ptr<Node> myArgMax = ArgMax(0);
            auto op = std::static_pointer_cast<OperatorTensor>(myArgMax -> getOperator());
            op->associateInput(0,myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            fmt::print("{:.^20}\n", "forward");
            myArgMax->forward();
            fmt::print("{:.^20}\n", "result");
            op->getOutput(0)->print();
            fmt::print("{:.^20}\n", "truth");
            myOutput.print();

            REQUIRE(*(op->getOutput(0)) == myOutput);
        }
    }
    SECTION("Select_Last_Index") {
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array1D<float,10> {
            {
                1.0, 5.0, 9.0, 0.0, 6.0, 2.0, 9.0, 4.0, 3.0, 9.0
            }
        });
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array1D<float,1> {{9}});

        std::shared_ptr<Node> myArgMax = ArgMax(0, 1, 1);
        auto op = std::static_pointer_cast<OperatorTensor>(myArgMax -> getOperator());
        op->associateInput(0,myInput);
        op->setDataType(DataType::Float32);
        op->setBackend("cpu");
        myArgMax->forward();
        op->getOutput(0)->print();

        REQUIRE(*(op->getOutput(0)) == *myOutput);

    }
}