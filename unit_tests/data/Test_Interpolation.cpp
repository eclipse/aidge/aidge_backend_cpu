/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <aidge/backend/cpu/data/Interpolation.hpp>
#include <aidge/data/Interpolation.hpp>
#include <aidge/data/Tensor.hpp>
#include <aidge/filler/Filler.hpp>
#include <aidge/utils/Types.h>
#include <catch2/catch_test_macros.hpp>
#include <limits>

#include "aidge/backend/cpu/data/Interpolation.hpp"

namespace Aidge {

TEST_CASE("Interpolation", "[Interpolation][Data]") {

    SECTION("Linear") {
        std::set<Interpolation::Point<int>> pointsToInterpolateInt;
        std::set<Interpolation::Point<float>> pointsToInterpolateFloat;

        SECTION("1D") {
            pointsToInterpolateInt =
                std::set<Interpolation::Point<int>>({{{0}, 10}, {{1}, 20}});
            CHECK(abs(InterpolationCPU::linear({0.5}, pointsToInterpolateInt) -
                      15) <= std::numeric_limits<int>::epsilon());

            pointsToInterpolateFloat = std::set<Interpolation::Point<float>>(
                {{{0}, .0F}, {{1}, 0.2F}});
            CHECK(fabs(InterpolationCPU::linear({0.3},
                                                pointsToInterpolateFloat) -
                       .06F) <= 1e-5);
        }
        SECTION("2D") {
            // example taken from
            // https://en.wikipedia.org/wiki/Bilinear_interpolation
            pointsToInterpolateFloat = {{{14, 20}, 91.F},
                                        {{14, 21}, 162.F},
                                        {{15, 20}, 210.F},
                                        {{15, 21}, 95.F}};
            CHECK(fabs(InterpolationCPU::linear<float>(
                           {14.5F, 20.2F},
                           pointsToInterpolateFloat) -
                       146.1) < 1e-5);
            // pointsToInterpolateFloat = {{{0, 0}, .10F},
            //                             {{0, 1}, .20F},
            //                             {{1, 0}, .30F},
            //                             {{1, 1}, .40F}};
            // CHECK(abs(InterpolationCPU::linear<float>({1.5, 0.5},
            //                                         pointsToInterpolateInt)
            //                                         -
            //           25) < std::numeric_limits<int>::epsilon());

            // pointsToInterpolateFloat = std::vector({0.1F, 0.2F, 0.3F,
            // 0.4F}); CHECK(InterpolationCPU::linear(pointsToInterpolateFloat)
            // == .25f);
        }
        SECTION("3D") {
            pointsToInterpolateFloat = {{{0, 0, 0}, .1F},
                                        {{0, 0, 1}, .2F},
                                        {{0, 1, 0}, .3F},
                                        {{0, 1, 1}, .4F},
                                        {{1, 0, 0}, .5F},
                                        {{1, 0, 1}, .6F},
                                        {{1, 1, 0}, .7F},
                                        {{1, 1, 1}, .8F}};
            CHECK(fabs(InterpolationCPU::linear({.5, .5, .5},
                                                pointsToInterpolateFloat) -
                       .45f) < 1e-5);
        }
        SECTION("4D") {
            SECTION("Casual") {
                pointsToInterpolateFloat = {{{0, 0, 0, 0}, .1F},
                                            {{0, 0, 0, 1}, .2F},
                                            {{0, 0, 1, 0}, .3F},
                                            {{0, 0, 1, 1}, .4F},
                                            {{0, 1, 0, 0}, .5F},
                                            {{0, 1, 0, 1}, .6F},
                                            {{0, 1, 1, 0}, .7F},
                                            {{0, 1, 1, 1}, .8F},
                                            {{1, 0, 0, 0}, .9F},
                                            {{1, 0, 0, 1}, 1.F},
                                            {{1, 0, 1, 0}, 1.1F},
                                            {{1, 0, 1, 1}, 1.2F},
                                            {{1, 1, 0, 0}, 1.3F},
                                            {{1, 1, 0, 1}, 1.4F},
                                            {{1, 1, 1, 0}, 1.5F},
                                            {{1, 1, 1, 1}, 1.6F}};
                CHECK(fabs(InterpolationCPU::linear<float>(
                               {.5, .5, .5, .5},
                               pointsToInterpolateFloat) -
                           .85f) < 0.0001);
            }
        }
        SECTION("Some of the coords to interpolate were round") {
            // In this case retrieveNeighbours()
            //  only retrieved the neighbours against not round dimensions
            auto tensor =
                std::make_shared<Tensor>(std::vector<DimSize_t>({10, 10}));
            tensor->setDataType(DataType::Float32);
            tensor->setBackend("cpu");
            Aidge::constantFiller(tensor, 1337.F);

            std::set<Interpolation::Point<float>> expectedResult = {
                {{0, 0, -1, -1}, 0.F},
                {{0, 0, 0, -1}, 0.F},
                {{0, 0, -1, 0}, 0.F},
                {{0, 0, 0, 0}, 1337.F}};

            pointsToInterpolateFloat = Interpolation::retrieveNeighbours(
                reinterpret_cast<float *>(tensor->getImpl()->rawPtr()),
                tensor->dims(),
                std::vector<float>({0.F, 0.F, -0.25F, -0.25F}));

            pointsToInterpolateFloat = {{{0, 0, -1, -1}, 1337.F},
                                        {{0, 0, 0, -1}, 1337.F},
                                        {{0, 0, -1, 0}, 1337.F},
                                        {{0, 0, 0, 0}, 1337.F}};
        }
    }
    SECTION("Nearest") {
        std::set<Interpolation::Point<float>> pointsToInterpolate;
        std::vector<float> coordToInterpolate;
        SECTION("1D") {
            coordToInterpolate = {0.5F};
            pointsToInterpolate =
                std::set<Interpolation::Point<float>>{{{0}, 1.0F},
                                                      {{1}, 2.0F},
                                                      {{2}, 3.0F},
                                                      {{3}, 4.0F},
                                                      {{4}, 5.0F}};

            SECTION("Floor") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::Floor) == 1);
            }
            SECTION("Ceil") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::Ceil) == 2);
            }
            SECTION("RoundPreferFloor") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::RoundPreferFloor) == 1);
            }
            SECTION("RoundPreferCeil") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::RoundPreferCeil) == 2);
            }
        }
        SECTION("2D") {
            coordToInterpolate = {2.5F, 3.97F};
            pointsToInterpolate = {{{0, 0}, 10.0},
                                   {{1, 1}, 20.0},
                                   {{2, 3}, 30.0},
                                   {{2, 4}, 40.0},
                                   {{3, 3}, 50.0},
                                   {{3, 4}, 60.0}};
            SECTION("Floor") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::Floor) == 30.);
            }
            SECTION("Ceil") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::Ceil) == 60.);
            }
            SECTION("RoundPreferFloor") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::RoundPreferFloor) ==
                      40.);
            }
            SECTION("RoundPreferCeil") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::RoundPreferCeil) == 60.);
            }
        }
        SECTION("3D") {
            coordToInterpolate = {1.9, 2.1, 3.6};
            pointsToInterpolate = {{{0, 0, 0}, 5.0},
                                   {{1, 2, 3}, 10.0},
                                   {{2, 1, 4}, 20.0},
                                   {{2, 2, 4}, 30.0},
                                   {{2, 3, 3}, 40.0},
                                   {{2, 3, 4}, 50.0},
                                   {{3, 3, 4}, 60.0}};
            SECTION("Floor") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::Floor) == 10.);
            }
            SECTION("Ceil") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::Ceil) == 50.);
            }
            SECTION("RoundPreferFloor") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::RoundPreferFloor) ==
                      30.);
            }
            SECTION("RoundPreferCeil") {
                CHECK(InterpolationCPU::nearest(
                          coordToInterpolate,
                          pointsToInterpolate,
                          Interpolation::Mode::RoundPreferCeil) == 30.);
            }
        }
    }
}
} // namespace Aidge
