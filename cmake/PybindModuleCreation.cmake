function(generate_python_binding pybind_module_name target_to_bind) 

    find_package(Python COMPONENTS Interpreter Development.Module)

    Include(FetchContent)

    set(PYBIND_VERSION v2.10.4)
    message(STATUS "Retrieving pybind ${PYBIND_VERSION} from git")

    FetchContent_Declare(
        PyBind11
        GIT_REPOSITORY https://github.com/pybind/pybind11.git
        GIT_TAG        ${PYBIND_VERSION} # or a later release
    )

    FetchContent_MakeAvailable(PyBind11)

    message(STATUS "Creating binding for module ${pybind_module_name}")
    file(GLOB_RECURSE pybind_src_files "python_binding/*.cpp")

    pybind11_add_module(${pybind_module_name} MODULE ${pybind_src_files} "NO_EXTRAS") # NO EXTRA recquired for pip install
    target_include_directories(${pybind_module_name} PRIVATE "python_binding")
    target_link_libraries(${pybind_module_name} PRIVATE ${target_to_bind})
endfunction()
