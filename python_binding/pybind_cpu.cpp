#include <pybind11/pybind11.h>
// Need to call this header to register every impl
#include "aidge/backend/cpu.hpp"

namespace py = pybind11;

namespace Aidge {

void init_CpuVersionInfo(py::module& m);

void init_Aidge(py::module& m){
    init_CpuVersionInfo(m);
}


PYBIND11_MODULE(aidge_backend_cpu, m) {
    init_Aidge(m);
}
}
