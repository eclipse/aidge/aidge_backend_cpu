#include <pybind11/pybind11.h>
#include "aidge/utils/sys_info/CpuVersionInfo.hpp"

namespace py = pybind11;
namespace Aidge {
void init_CpuVersionInfo(py::module& m){
    m.def("show_version", &showBackendCpuVersion);
    m.def("get_project_version", &getBackendCPUProjectVersion);
    m.def("get_git_hash", &getBackendCPUGitHash);
}
}
