/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/AbsImpl.hpp"

#include <memory>
#include <vector>

#include "aidge/backend/cpu/operator/AbsImpl_kernels.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Abs.hpp"
#include "aidge/utils/Types.h"

template <>
void Aidge::AbsImpl_cpu::forward() {
    const Abs_Op& op = static_cast<const Abs_Op&>(mOp);

    // Find the correct kernel type
    const auto impl = Registrar<AbsImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(
        op.getInput(0)->size(),
        op.getInput(0)->getImpl()->rawPtr(),
        op.getOutput(0)->getImpl()->rawPtr()
    );
}

template <>
void Aidge::AbsImpl_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for Abs_Op on backend cpu");
}
