/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/LeakyReLUImpl.hpp"

#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/LeakyReLUImpl_kernels.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/LeakyReLU.hpp"
#include "aidge/utils/Log.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/Registrar.hpp"

template <>
void Aidge::LeakyReLUImpl_cpu::forward() {
    const LeakyReLU_Op& op_ = dynamic_cast<const LeakyReLU_Op&>(mOp);

    std::shared_ptr<Tensor> in0 = op_.getInput(0);
    std::shared_ptr<Tensor> out0 = op_.getOutput(0);
    AIDGE_ASSERT(in0, "missing input #0");

    // Find the correct kernel type
    const auto impl = Registrar<LeakyReLUImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.negativeSlope(),
        in0->size(),
        getCPUPtr(mOp.getRawInput(0)),
        getCPUPtr(mOp.getRawOutput(0)));
}

template <>
void Aidge::LeakyReLUImpl_cpu::backward() {
    // reversing in and out Data for backprop
    const LeakyReLU_Op& op_ = dynamic_cast<const LeakyReLU_Op&>(mOp);
    std::shared_ptr<Tensor> in0  = op_.getOutput(0)->grad();
    std::shared_ptr<Tensor> out0 = op_.getInput(0)->grad();
    AIDGE_ASSERT(in0, "missing input #0");

    // Find the correct kernel type
    const auto impl = Registrar<LeakyReLUImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.backward(op_.negativeSlope(),
        in0->size(),
        getCPUPtr(in0),
        getCPUPtr(out0));
}