/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/ConstantOfShapeImpl.hpp"

#include <functional>
#include <memory>
#include <vector>

#include "aidge/backend/cpu/operator/ConstantOfShapeImpl_kernels.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/ConstantOfShape.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

template <>
void Aidge::ConstantOfShapeImpl_cpu::forward() {
  const ConstantOfShape_Op &op_ = static_cast<const ConstantOfShape_Op &>(mOp);
  // Check if input is provided
  AIDGE_ASSERT(op_.getInput(0), "{} : Missing input 0", __func__);

    // Find the correct kernel type
    const auto impl = Registrar<ConstantOfShapeImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.getOutput(0)->dims(),
             op_.value(), 
             op_.getOutput(0)->getImpl()->rawPtr());
}

template <>
void Aidge::ConstantOfShapeImpl_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for ConstantOfShape_Op on backend cpu");
}
