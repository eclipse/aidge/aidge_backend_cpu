/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/AvgPoolingImpl.hpp"

#include <array>
#include <numeric>
#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/AvgPoolingImpl_kernels.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/utils/Types.h"

template <>
void Aidge::AvgPoolingImpl2D_cpu::forward() {
    const auto& op_ = dynamic_cast<const AvgPooling_Op<2>&>(mOp);
    assert(op_.getInput(0) && "missing input #0");

    // Find the correct kernel type
    const auto impl = Registrar<AvgPoolingImpl2D_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.strideDims(),
               op_.kernelDims(),
               op_.getInput(0)->template dims<4>(),
               getCPUPtr(op_.getInput(0)),
               getCPUPtr(op_.getOutput(0)));
}

template <>
void Aidge::AvgPoolingImpl2D_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for AvgPooling_Op<2> on backend cpu");
}

