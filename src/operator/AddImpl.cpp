/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/AddImpl.hpp"

#include <cassert>
#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/AddImpl_kernels.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/ErrorHandling.hpp"

template <>
void  Aidge::AddImpl_cpu::forward() {
    const Add_Op& op = static_cast<const Add_Op&>(mOp);
    // Check inputs
    AIDGE_ASSERT(op.getInput(0), "missing input in Add operator");
    AIDGE_ASSERT(op.getInput(0)->hasImpl(), "cannot run Add forward because the 0-th input has no implementation.");

    AIDGE_ASSERT(op.getInput(1), "missing input in Add operator");
    AIDGE_ASSERT(op.getInput(1)->hasImpl(), "cannot run Add forward because the 1st input has no implementation.");

    AIDGE_ASSERT(op.getInput(1)->dataType() == op.getInput(0)->dataType(), "Cannot add inputs with two differents data type.");

    // Find the correct kernel type
    const auto impl = Registrar<AddImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Convert input data (no overhead if not needed!)
    // TODO: right now, if needed, memory will be allocated/deallocated at each
    // call to forward(). We might put the following shared_ptr as members of
    // this class to avoid that.
    std::shared_ptr<Tensor> input0Fallback, input1Fallback, input2Fallback;
    const auto& input0 = op.getInput(0)->refCastFrom(input0Fallback, *op.getInput(0));
    const auto& input1 = op.getInput(1)->refCastFrom(input1Fallback, *op.getInput(1));


    impl.forward(op.getInput(0)->dims(),
                op.getInput(1)->dims(),
                op.getOutput(0)->dims(),
                input0.getImpl()->rawPtr(),
                input1.getImpl()->rawPtr(),
                getCPUPtr(op.getRawOutput(0)));
}

template <>
void Aidge::AddImpl_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for Add_Op on backend cpu");
}
