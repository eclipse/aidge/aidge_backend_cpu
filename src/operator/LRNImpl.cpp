/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/operator/LRN.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

#include "aidge/backend/cpu/operator/LRNImpl.hpp"
#include "aidge/backend/cpu/operator/LRNImpl_kernels.hpp"

template <>
void Aidge::LRNImpl_cpu::forward() {
    const auto& op_ = dynamic_cast<const LRN_Op&>(mOp);
    AIDGE_ASSERT(!op_.getInput(0)->empty(), "LRN input empty");

    // Find the correct kernel type
    const auto impl = Registrar<LRNImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.alpha(),
               op_.beta(),
               op_.bias(),
               op_.size(),
               std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dims(),
               std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->getImpl()->rawPtr(),
               std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->getImpl()->rawPtr());
}

template <>
void Aidge::LRNImpl_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for LRN_Op on backend cpu");
}
