/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/operator/Conv.hpp"

#include "aidge/backend/cpu/operator/PadImpl.hpp"
#include "aidge/backend/cpu/operator/PadImpl_kernels.hpp"

Aidge::Elts_t Aidge::Pad_ProdConso_cpu::getNbRequiredProtected(Aidge::IOIndex_t inputIdx) const {
    AIDGE_ASSERT(inputIdx == 0, "input index out of range."
        "{} Operator has only one input", mOp.type());
    (void) inputIdx;


    // Padding cannot be in-place!
    // We must ensure that we do not override data that has not been consummed yet.
    const auto inputSize = std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->size();
    const auto outputSize = std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->size();
    return Elts_t::DataElts(outputSize - inputSize);
}

template <>
void Aidge::PadImpl1D_cpu::forward() {
    const auto& op_ = dynamic_cast<const Pad_Op<1>&>(mOp);
    AIDGE_ASSERT(op_.getInput(0), "missing input #0 in Pad Operator.");

    // Find the correct kernel type
    const auto impl = Registrar<PadImpl1D_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.beginEndBorders(),
                op_.borderType(),
                op_.borderValue(),
                op_.getInput(0)->template dims<3>(),
                getCPUPtr(mOp.getRawInput(0)),
                getCPUPtr(mOp.getRawOutput(0)));
}

template <>
void Aidge::PadImpl1D_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for Pad_Op<1> on backend cpu");
}

template <>
void Aidge::PadImpl2D_cpu::forward() {
    const auto& op_ = dynamic_cast<const Pad_Op<2>&>(mOp);
    AIDGE_ASSERT(op_.getInput(0), "missing input #0 in Pad Operator.");

    // Find the correct kernel type
    const auto impl = Registrar<PadImpl2D_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.beginEndBorders(),
                op_.borderType(),
                op_.borderValue(),
                op_.getInput(0)->template dims<4>(),
                getCPUPtr(mOp.getRawInput(0)),
                getCPUPtr(mOp.getRawOutput(0)));
}

template <>
void Aidge::PadImpl2D_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for Pad_Op<2> on backend cpu");
}
