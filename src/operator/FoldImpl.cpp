/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/operator/Conv.hpp"

#include "aidge/backend/cpu/operator/FoldImpl.hpp"
#include "aidge/backend/cpu/operator/FoldImpl_kernels.hpp"

template <>
void Aidge::FoldImpl2D_cpu::forward() {
    const auto& op_ = static_cast<const Fold_Op<2>&>(mOp);
    assert(std::static_pointer_cast<Tensor>(mOp.getRawInput(0)) && "missing input #0");

    // Find the correct kernel type
    const auto impl = Registrar<FoldImpl2D_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.outputDims(),
                op_.strideDims(),
                op_.dilationDims(),
                op_.kernelDims(),
                std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dims(),
                getCPUPtr(mOp.getRawInput(0)),
                getCPUPtr(mOp.getRawOutput(0)));
}

template <>
void Aidge::FoldImpl2D_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for Fold_Op<2> on backend cpu");
}
