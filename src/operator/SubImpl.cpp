/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/operator/Sub.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/Broadcasting.hpp"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

#include "aidge/backend/cpu/operator/SubImpl.hpp"
#include "aidge/backend/cpu/operator/SubImpl_kernels.hpp"

template <>
void Aidge::SubImpl_cpu::forward() {
    const Sub_Op& op_ = dynamic_cast<const Sub_Op&>(mOp);

    // Find the correct kernel type
    const auto impl = Registrar<SubImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.getInput(0)->dims(),
        op_.getInput(1)->dims(),
        op_.getOutput(0)->dims(),
        getCPUPtr(mOp.getRawInput(0)),
        getCPUPtr(mOp.getRawInput(1)),
        getCPUPtr(mOp.getRawOutput(0)));
}

template <>
void Aidge::SubImpl_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for Sub_Op on backend cpu");
}
