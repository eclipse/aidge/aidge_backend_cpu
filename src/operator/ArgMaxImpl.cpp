/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/ArgMaxImpl.hpp"

#include <memory>
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/operator/ArgMax.hpp"
#include "aidge/backend/cpu/operator/ArgMaxImpl_kernels.hpp"

template <>
void Aidge::ArgMaxImpl_cpu::forward() {
    const ArgMax_Op& op_ = dynamic_cast<const ArgMax_Op&>(mOp);

    // Find the correct kernel type
    const auto impl = Registrar<ArgMaxImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.axis(),
                op_.selectLastIndex(),
                op_.getInput(0)->dims(),
                op_.getInput(0)->getImpl()->rawPtr(),
                op_.getOutput(0)->getImpl()->rawPtr());
}

template <>
void Aidge::ArgMaxImpl_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for ArgMax_Op on backend cpu");
}
