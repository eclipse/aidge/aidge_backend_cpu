/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/operator/Mul.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/Broadcasting.hpp"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

#include "aidge/backend/cpu/operator/MulImpl.hpp"
#include "aidge/backend/cpu/operator/MulImpl_kernels.hpp"

template <>
void Aidge::MulImpl_cpu::forward() {
    const Mul_Op& op_ = dynamic_cast<const Mul_Op&>(mOp);

    // Find the correct kernel type
    const auto impl = Registrar<MulImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.getInput(0)->dims(),
        op_.getInput(1)->dims(),
        op_.getOutput(0)->dims(),
        getCPUPtr(mOp.getRawInput(0)),
        getCPUPtr(mOp.getRawInput(1)),
        getCPUPtr(mOp.getRawOutput(0)));
}

template <>
void Aidge::MulImpl_cpu::backward() {
    const Mul_Op& op_ = dynamic_cast<const Mul_Op&>(mOp);

    auto in0 = op_.getInput(0);
    auto in1 = op_.getInput(1);
    auto in0grad = op_.getInput(0)->grad();
    auto in1grad = op_.getInput(1)->grad();
    auto out0grad = op_.getOutput(0)->grad();

    // Find the correct kernel type
    const auto impl = Registrar<MulImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.backward(/* input0Length */ in0grad->size(),
               /* input1Length */ in1grad->size(),
               /* grad0Length  */ out0grad->size(),
               /* input0Dims   */ in0->dims(),
               /* input1Dims   */ in1->dims(),
               out0grad->dims(),
               getCPUPtr(in0),
               getCPUPtr(in1),
               getCPUPtr(out0grad),
               getCPUPtr(in0grad),
               getCPUPtr(in1grad));
}
