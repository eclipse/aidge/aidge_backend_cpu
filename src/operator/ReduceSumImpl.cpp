/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/ReduceSumImpl.hpp"

#include <memory>
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/operator/ReduceSum.hpp"
#include "aidge/backend/cpu/operator/ReduceSumImpl_kernels.hpp"

template <>
void Aidge::ReduceSumImpl_cpu::forward() {
    const ReduceSum_Op& op_ = dynamic_cast<const ReduceSum_Op&>(mOp);

    // Find the correct kernel type
    const auto impl = Registrar<ReduceSumImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(op_.axes(),
                op_.keepDims(),
                op_.getInput(0)->dims(),
                op_.getInput(0)->getImpl()->rawPtr(),
                op_.getOutput(0)->getImpl()->rawPtr());
}

template <>
void Aidge::ReduceSumImpl_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for ReduceSum_Op on backend cpu");
}
