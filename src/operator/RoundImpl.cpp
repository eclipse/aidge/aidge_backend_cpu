/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Round.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/operator/RoundImpl.hpp"
#include "aidge/backend/cpu/operator/RoundImpl_kernels.hpp"

template <>
void Aidge::RoundImpl_cpu::forward() {
    std::shared_ptr<Tensor> in0 = std::static_pointer_cast<Tensor>(mOp.getRawInput(0));
    std::shared_ptr<Tensor> out0 = std::static_pointer_cast<Tensor>(mOp.getRawOutput(0));
    AIDGE_ASSERT(in0, "missing input #0");

    // Find the correct kernel type
    const auto impl = Registrar<RoundImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(in0->size(),
        getCPUPtr(mOp.getRawInput(0)),
        getCPUPtr(mOp.getRawOutput(0)));
}
template <>
void Aidge::RoundImpl_cpu::backward() {
    AIDGE_THROW_OR_ABORT(std::runtime_error, "Backward not yet implemented for Round_Op on backend cpu");
}