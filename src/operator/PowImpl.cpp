/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/operator/Pow.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/Broadcasting.hpp"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

#include "aidge/backend/cpu/operator/PowImpl.hpp"
#include "aidge/backend/cpu/operator/PowImpl_kernels.hpp"

template <>
void Aidge::PowImpl_cpu::forward() {

    const Pow_Op& op = static_cast<const Pow_Op&>(mOp);
    // Check inputs
    AIDGE_ASSERT(op.getInput(0), "missing input in Pow operator");
    AIDGE_ASSERT(op.getInput(0)->hasImpl(), "cannot run Pow forward because the 0-th input has no implementation.");

    AIDGE_ASSERT(op.getInput(1), "missing input in Pow operator");
    AIDGE_ASSERT(op.getInput(1)->hasImpl(), "cannot run Pow forward because the 1st input has no implementation.");

    AIDGE_ASSERT(op.getInput(1)->dataType() == op.getInput(0)->dataType(), "Cannot compute Pow with inputs of two differents data type.");

    // Find the correct kernel type
    const auto impl = Registrar<PowImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Convert input data (no overhead if not needed!)
    // TODO: right now, if needed, memory will be allocated/deallocated at each
    // call to forward(). We might put the following shared_ptr as members of
    // this class to avoid that.
    std::shared_ptr<Tensor> input0Fallback, input1Fallback, input2Fallback;
    const auto& input0 = op.getInput(0)->refCastFrom(input0Fallback, *op.getInput(0));
    const auto& input1 = op.getInput(1)->refCastFrom(input1Fallback, *op.getInput(1));


    impl.forward(op.getInput(0)->dims(),
                op.getInput(1)->dims(),
                op.getOutput(0)->dims(),
                input0.getImpl()->rawPtr(),
                input1.getImpl()->rawPtr(),
                getCPUPtr(op.getRawOutput(0)));

}

template <>
void Aidge::PowImpl_cpu::backward() {
    const Pow_Op& op_ = dynamic_cast<const Pow_Op&>(mOp);

    auto in0 = op_.getInput(0);
    auto in1 = op_.getInput(1);
    auto in0grad = op_.getInput(0)->grad();
    auto in1grad = op_.getInput(1)->grad();
    auto out0grad = op_.getOutput(0)->grad();

    const std::vector<std::size_t> input0gradDims = getBroadcastedDims(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->grad()->dims(),
                                                                       std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->grad()->dims());
    const std::vector<std::size_t> input1gradDims = getBroadcastedDims(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->grad()->dims(),
                                                                       std::static_pointer_cast<Tensor>(mOp.getRawInput(1))->grad()->dims());

    // Find the correct kernel type
    const auto impl = Registrar<PowImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.backward(input0gradDims,
                input1gradDims,
                out0grad->dims(),
                getCPUPtr(in0),
                getCPUPtr(in1),
                getCPUPtr(out0grad),
                getCPUPtr(in0grad),
                getCPUPtr(in1grad));
}
