/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
#include "aidge/backend/cpu/operator/ResizeImpl.hpp"
#include "aidge/backend/cpu/operator/ResizeImpl_kernels.hpp"
#include "aidge/operator/Resize.hpp"

#include <cassert>
#include <cstdint>
#include <sys/stat.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/utils/ErrorHandling.hpp"

namespace Aidge {

template <> void ResizeImpl_cpu::forward() {
    auto &op = dynamic_cast<const Resize_Op &>(mOp);

    /** @brief input #0 */
    int8_t idxData = 0;

    const bool input0DataPresent =
        op.getInput(idxData) && !op.getInput(idxData)->undefined();

    ///////////////////////////////////////
    // CHECKING NODE CONNECTIONS
    AIDGE_ASSERT(input0DataPresent, "{}: missing data input #0", op.type());

    ///////////////////////////////////////
    // CALL TO FORWARD
    const auto impl =
        Registrar<ResizeImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    impl.forward(op.getInput(idxData)->getImpl()->rawPtr(),
                 op.getInput(idxData)->dims(),
                 op.getOutput(0)->dims(),

                 op.coordinateTransformationMode(),
                 op.interpolationMode(),
                 op.paddingMode(),

                 op.getOutput(0)->getImpl()->rawPtr() // output pointer
    );
}

template <> void Aidge::ResizeImpl_cpu::backward() {
    AIDGE_THROW_OR_ABORT(
        std::runtime_error,
        "Backward not yet implemented for Slice_Op on backend cpu");
}
} // namespace Aidge
