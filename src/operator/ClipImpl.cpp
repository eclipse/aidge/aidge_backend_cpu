/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Clip.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/utils/ErrorHandling.hpp"

#include "aidge/backend/cpu/operator/ClipImpl.hpp"
#include "aidge/backend/cpu/operator/ClipImpl_kernels.hpp"

template<>
void Aidge::ClipImpl_cpu::forward() {

	const Clip_Op& op_ = dynamic_cast<const Clip_Op&>(mOp);
    std::shared_ptr<Tensor> in0 = op_.getInput(0);
    std::shared_ptr<Tensor> out0 = op_.getOutput(0);
    AIDGE_ASSERT(in0, "missing input #0");
    /*AIDGE_ASSERT(in1, "missing input #1 -> Min value empty shape Tensor");
    AIDGE_ASSERT(in2, "missing input #2 -> Max value empty shape Tensor");*/
    // Find the correct kernel type
    const auto impl = Registrar<ClipImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(
       op_.min(),
       op_.max(),
       getCPUPtr(mOp.getRawInput(0)), 
       in0->size(), 
       getCPUPtr(mOp.getRawOutput(0))
    );
}

template<>
void Aidge::ClipImpl_cpu::backward() {

    const Clip_Op& op_ = dynamic_cast<const Clip_Op&>(mOp);
    std::shared_ptr<Tensor> in0  = op_.getInput(0);
    std::shared_ptr<Tensor> out0  = op_.getOutput(0);
    std::shared_ptr<Tensor> gra_in0 = op_.getInput(0)->grad();
    std::shared_ptr<Tensor> gra_out0 = op_.getOutput(0)->grad();    
    AIDGE_ASSERT(out0, "missing output #0 for current {} operator", op_.type());
    
    // Find the correct kernel type
    const auto impl = Registrar<ClipImpl_cpu>::create(getBestMatch(getRequiredSpec()));
    // Call kernel
    impl.backward(
        op_.min(),
        op_.max(),
        gra_in0->size(), 
        getCPUPtr(in0), 
        getCPUPtr(gra_out0), 
        getCPUPtr(gra_in0)
    );
}
