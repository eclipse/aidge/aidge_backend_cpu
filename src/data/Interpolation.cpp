/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/data/Interpolation.hpp"

#include <aidge/utils/Log.hpp>
#include <algorithm>
#include <cmath>
#include <cstdint>

#include <iterator>
#include <stdexcept>
#include <utility>
#include <vector>

#include <aidge/data/Interpolation.hpp>
#include <aidge/data/half.hpp>
#include <aidge/utils/ErrorHandling.hpp>
#include <aidge/utils/Types.h>

namespace Aidge {

template <typename T>
std::set<Interpolation::Point<T>>
InterpolationCPU::linearRecurse(const std::vector<float> &coordToInterpolate,
                                const std::set<Point<T>> &points,
                                const DimIdx_t alongDim) {

    // all points have been discriminated properly along given dimension.
    if (points.size() == 1) {
        return points;
    }

    auto extractPtCoords = [](std::set<Point<T>> pts) -> std::set<Coords> {
        std::set<Coords> result;
        for (const auto &pt : pts) {
            result.insert(pt.first);
        }
        return result;
    };
    ///////////////////
    // ERROR CHECKING
    if (alongDim > coordToInterpolate.size() || points.size() == 0) {
        // retrieving points coords as points values can be in half_float &
        // this type is not fmt compatible
        std::vector<Coords> pointsCoords;
        for (const auto &point : points) {
            pointsCoords.push_back(point.first);
        }
        AIDGE_ASSERT(
            alongDim >= coordToInterpolate.size(),
            "InterpolationCPU::linearInterpolationRecurse: alongDim value "
            "exceeded exceeded number of dimensions of coordsTointerpolate. "
            "Interpolation has failed. Input values : \n - "
            "coordsToInterpolate {}\n - pointsToInterpolate {}\n - alongDim "
            "{}",
            coordToInterpolate,
            pointsCoords,
            alongDim);
        AIDGE_ASSERT(
            points.size() == 0,
            "InterpolationCPU::linearInterpolationRecurse: entering recursive "
            "function with 0 points. Interpolation has failed."
            "Please file a bug report to aidge_backend_cpu repo: "
            "https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/-/"
            "issues."
            "\nInput values : \n - "
            "coordsToInterpolate {}\n - pointsToInterpolate {}\n - alongDim "
            "{}",
            coordToInterpolate,
            pointsCoords,
            alongDim);
    }
    Log::debug("\nEntering linear recurse with {} points.", points.size());
    Log::debug("Points : {}", extractPtCoords(points));
    Log::debug("coordsToInterpolate : {}", coordToInterpolate);
    Log::debug("alongDim : {}", alongDim);

    ///////////////////
    // COMPUTATION
    // split  all points along each dimension
    // depending on if their coords[alongDim] are above or under
    // coords to interpolate values
    std::set<Point<T>> lowerPoints;
    std::set<Point<T>> upperPoints;
    for (const auto &point : points) {
        if (point.first[alongDim] <= coordToInterpolate[alongDim]) {
            lowerPoints.insert(point);
        } else {
            upperPoints.insert(point);
        }
    }
    Log::debug("alongDim : {}", alongDim);
    Log::debug("lowerPoints : {}", extractPtCoords(lowerPoints));
    Log::debug("upperPoints : {}", extractPtCoords(upperPoints));

    // Here are 3 cases
    // 1. upper/lowerPoints.size() == 0
    //        Coordinates to interpolate along current dimension are round.
    //        That would be equivalent to a linear interpolation with a
    //        ponderation of 1 for lowerPoints & 0 for upperPoints(or the
    //        opposite idk), hence we will only take lower/upperPoints values
    //        from there.
    //
    //        Why this happens :
    //        If coordinates are round, the floor()/ceil() operations called
    //        in retrieveNeighbours to generate direct neighbours of floating
    //        coordinates returned the same value.
    //
    // 2. lower/upperPoints.size() == 1
    //        All dimensions have been discriminated, we can proceed to
    //        weighted interpolation
    //
    // 3. lower/upperPoints.size() > 1
    //        points have not been all discriminated and must be further split
    //        so we call linearRecurse()
    switch (lowerPoints.size()) {
        case 0: {
            return linearRecurse(coordToInterpolate, upperPoints, alongDim + 1);
        }
        case 1: {
            break;
        }
        default: {
            lowerPoints =
                linearRecurse(coordToInterpolate, lowerPoints, alongDim + 1);
            break;
        }
    }

    switch (upperPoints.size()) {
        case 0: {
            return linearRecurse(coordToInterpolate, lowerPoints, alongDim + 1);
        }
        case 1: {
            break;
        }
        default: {
            upperPoints =
                linearRecurse(coordToInterpolate, upperPoints, alongDim + 1);
            break;
        }
    }

    // At this point lowerPoints & upperPoints are garanteed to be
    // 1 sized arrays
    AIDGE_ASSERT(lowerPoints.size() == 1,
                 "LowerPoints Size = {} != 1",
                 lowerPoints.size());
    AIDGE_ASSERT(upperPoints.size() == 1,
                 "upperPoints Size = {} != 1",
                 upperPoints.size());

    //     ( point[dim] - Pl[dim] )
    // t = ------------------------
    //      ( Pu[dim] - Pl[dim] )
    float weight =
        (coordToInterpolate[alongDim] - lowerPoints.begin()->first[alongDim]) /
        (upperPoints.begin()->first[alongDim] -
         lowerPoints.begin()->first[alongDim]);

    Point<T> interpolatedPoint = std::make_pair(
        lowerPoints.begin()->first,
        static_cast<T>((1.F - weight) * lowerPoints.begin()->second +
                       weight * upperPoints.begin()->second));
    // 0 is just a sanity check to ensure later that all dims have been
    // interpolate
    interpolatedPoint.first[alongDim] = 0;
    Log::debug("successfully returned from alongDim : {}", alongDim);
    return std::set<Point<T>>({interpolatedPoint});
}

template <typename T>
T InterpolationCPU::linear(const std::vector<float> &coordToInterpolate,
                           const std::set<Point<T>> &pointsToInterpolate) {

    auto result = linearRecurse(coordToInterpolate, pointsToInterpolate, 0);
    AIDGE_ASSERT(result.size() == 1,
                 "Result size is not 1 but {}",
                 result.size());
    // if (!std::all_of(result.begin()->first.begin(),
    //                  result.begin()->first.end(),
    //                  [](DimSize_t coord) -> bool { return coord == 0; })) {
    //     std::vector<Coords> ptCoords;
    //     std::transform(pointsToInterpolate.begin(),
    //                    pointsToInterpolate.end(),
    //                    std::back_inserter(ptCoords),
    //                    [](Point<T> pt) { return pt.first; });
    //     AIDGE_THROW_OR_ABORT(std::runtime_error,
    //                          "Not all dimensions have been interpolated."
    //                          "Input data :"
    //                          "\n\t coord to interpolate : {}"
    //                          "\n\t pointsToInterpolate : {}",
    //                          //        "\n\tAll non 0 values show dimensions
    //                          //        that were not interpolated : {}",
    //                          coordToInterpolate,
    //                          ptCoords //,
    //                                   // result.begin()->first
    //     );
    // }
    return result.begin()->second;
}

template <typename T>
T InterpolationCPU::nearest(const std::vector<float> &coordsToInterpolate,
                            const std::set<Point<T>> &points,
                            const Interpolation::Mode nearestMode) {

    AIDGE_ASSERT(
        coordsToInterpolate.size() == points.begin()->first.size(),
        "Interpolation::nearest(): dimension mismatch : coordinate "
        "to interpolate ({}) have not the same number of dimensions than "
        "the points to interpolate({}).",
        coordsToInterpolate,
        points.begin()->first);
    std::function<int64_t(const float &)> updateCoordinates;
    switch (nearestMode) {
    case Interpolation::Mode::Ceil: {
        updateCoordinates = [](const float &coord) -> int64_t {
            return ceil(coord);
        };
        break;
    }
    case Interpolation::Mode::Floor: {
        updateCoordinates = [](const float &coord) -> int64_t {
            return floor(coord);
        };
        break;
    }
    case Interpolation::Mode::RoundPreferFloor: {
        updateCoordinates = [](const float &coord) -> int64_t {
            return (coord - floor(coord)) == 0.5 ? floor(coord)
                                                 : std::round(coord);
        };
        break;
    }
    case Interpolation::Mode::RoundPreferCeil: {
        updateCoordinates = [](const float &coord) -> int64_t {
            return (coord - floor(coord)) == 0.5 ? ceil(coord)
                                                 : std::round(coord);
        };
        break;
    }
    default: {
        AIDGE_THROW_OR_ABORT(
            std::runtime_error,
            "Invalid Interpolation mode for "
            "InterpolationCPU::interpolateNearest. Accepted modes are : "
            "Ceil({}),Floor({}),RoundPreferCeil({}), "
            "RoundPreferFloor({}). Got {}.",
            static_cast<int>(Ceil),
            static_cast<int>(Floor),
            static_cast<int>(RoundPreferCeil),
            static_cast<int>(RoundPreferFloor),
            static_cast<int>(nearestMode));
    }
    }
    Coords nearestCoords;
    nearestCoords.reserve(coordsToInterpolate.size());
    for (const auto &coord : coordsToInterpolate) {
        nearestCoords.push_back(updateCoordinates(coord));
    }
    auto it = std::find_if(
        points.begin(),
        points.end(),
        [nearestCoords](auto &point) { return nearestCoords == point.first; });
    if (it != points.end()) {
        return it->second;
    } else {
        Log::warn("Interpolate::nearest(): did not find a fitting point in "
                  "the neighbours whose coordinates were {}, returning 0. "
                  "Available neighbours are at following indexes: ",
                  coordsToInterpolate);
        for (const auto &point : points) {
            Log::warn("idx : [{}]\t\tvalue {}", point.first);
        }
        return static_cast<T>(0);
    }
}

template <typename T>
T InterpolationCPU::interpolate(const std::vector<float> &coordsToInterpolate,
                                const std::set<Point<T>> &points,
                                const Mode interpMode) {

    T result{0};
    switch (interpMode) {
    case Interpolation::Mode::Cubic: {
        AIDGE_THROW_OR_ABORT(
            std::runtime_error,
            "Unsupported interpolation mode selected : Cubic.");
        break;
    }
    case Interpolation::Mode::Linear: {
        return linear(coordsToInterpolate, points);
        break;
    }
    case Interpolation::Mode::Ceil:
    case Interpolation::Mode::Floor:
    case Interpolation::Mode::RoundPreferFloor:
    case Interpolation::Mode::RoundPreferCeil: {
        result =
            InterpolationCPU::nearest(coordsToInterpolate, points, interpMode);
        break;
    }
    default: {
        AIDGE_THROW_OR_ABORT(std::runtime_error,
                             "InterpolationCPU::Interpolate({}): Unsupported "
                             "interpolation mode given as input.",
                             static_cast<int>(interpMode));
        break;
    }
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// TEMPLATE DECLARATION
//////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////
// INTERPOLATE
template int8_t InterpolationCPU::interpolate<int8_t>(
    const std::vector<float> &originalCoords,
    const std::set<Point<int8_t>> &points,
    const Mode interpMode);
template int16_t InterpolationCPU::interpolate<int16_t>(
    const std::vector<float> &originalCoords,
    const std::set<Point<int16_t>> &points,
    const Mode interpMode);
template int32_t InterpolationCPU::interpolate<int32_t>(
    const std::vector<float> &originalCoords,
    const std::set<Point<int32_t>> &points,
    const Mode interpMode);
template int64_t InterpolationCPU::interpolate<int64_t>(
    const std::vector<float> &originalCoords,
    const std::set<Point<int64_t>> &points,
    const Mode interpMode);

template half_float::half InterpolationCPU::interpolate<half_float::half>(
    const std::vector<float> &originalCoords,
    const std::set<Point<half_float::half>> &points,
    const Mode interpMode);
template float InterpolationCPU::interpolate<float>(
    const std::vector<float> &originalCoords,
    const std::set<Point<float>> &points,
    const Mode interpMode);
template double InterpolationCPU::interpolate<double>(
    const std::vector<float> &originalCoords,
    const std::set<Point<double>> &points,
    const Mode interpMode);

////////////////////////////////////////////////////////////////////
// INTERPOLATE LINEAR (& its associated recursive function)
template int8_t
InterpolationCPU::linear(const std::vector<float> &coordsToInterpolate,
                         const std::set<Point<int8_t>> &points);
template std::set<Interpolation::Point<int8_t>>
InterpolationCPU::linearRecurse(const std::vector<float> &coordsToInterpolate,
                                const std::set<Point<int8_t>> &points,
                                DimIdx_t alongDim);
template int16_t
InterpolationCPU::linear(const std::vector<float> &coordsToInterpolate,
                         const std::set<Point<int16_t>> &points);
template std::set<Interpolation::Point<int16_t>>
InterpolationCPU::linearRecurse(const std::vector<float> &coordsToInterpolate,
                                const std::set<Point<int16_t>> &points,
                                DimIdx_t alongDim);
template int32_t
InterpolationCPU::linear(const std::vector<float> &coordsToInterpolate,
                         const std::set<Point<int32_t>> &points);
template std::set<Interpolation::Point<int32_t>>
InterpolationCPU::linearRecurse(const std::vector<float> &coordsToInterpolate,
                                const std::set<Point<int32_t>> &points,
                                DimIdx_t alongDim);

template half_float::half
InterpolationCPU::linear(const std::vector<float> &coordsToInterpolate,
                         const std::set<Point<half_float::half>> &points);
template std::set<Interpolation::Point<half_float::half>>
InterpolationCPU::linearRecurse(
    const std::vector<float> &coordsToInterpolate,
    const std::set<Point<half_float::half>> &points,
    DimIdx_t alongDim);
template float
InterpolationCPU::linear(const std::vector<float> &coordsToInterpolate,
                         const std::set<Point<float>> &points);
template std::set<Interpolation::Point<float>>
InterpolationCPU::linearRecurse(const std::vector<float> &coordsToInterpolate,
                                const std::set<Point<float>> &points,
                                DimIdx_t alongDim);
template double
InterpolationCPU::linear(const std::vector<float> &coordsToInterpolate,
                         const std::set<Point<double>> &points);
template std::set<Interpolation::Point<double>>
InterpolationCPU::linearRecurse(const std::vector<float> &coordsToInterpolate,
                                const std::set<Point<double>> &points,
                                DimIdx_t alongDim);

//////////////////////////////////
// INTERPOLATE NEAREST
template int8_t
InterpolationCPU::nearest(const std::vector<float> &originalCoords,
                          const std::set<Point<int8_t>> &points,
                          const Interpolation::Mode nearestMode);
template int16_t
InterpolationCPU::nearest(const std::vector<float> &originalCoords,
                          const std::set<Point<int16_t>> &points,
                          const Interpolation::Mode nearestMode);
template int32_t
InterpolationCPU::nearest(const std::vector<float> &originalCoords,
                          const std::set<Point<int32_t>> &points,
                          const Interpolation::Mode nearestMode);

template half_float::half
InterpolationCPU::nearest(const std::vector<float> &originalCoords,
                          const std::set<Point<half_float::half>> &points,
                          const Interpolation::Mode nearestMode);
template float
InterpolationCPU::nearest(const std::vector<float> &originalCoords,
                          const std::set<Point<float>> &points,
                          const Interpolation::Mode nearestMode);
template double
InterpolationCPU::nearest(const std::vector<float> &originalCoords,
                          const std::set<Point<double>> &points,
                          const Interpolation::Mode nearestMode);

} // namespace Aidge
