![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/pipeline.svg?ignore_skipped=true) ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)

# Aidge CPU library

You can find in this folder the library that implements the CPU operators. <br>
Those operators can be used on any machine with an Linux OS.

[TOC]

## Installation

### Dependencies
- `GCC`
- `Make`/`Ninja`
- `CMake`
- `Python` (optional, if you have no intend to use this library in python with pybind)

#### Aidge dependencies
 - `aidge_core`

### Pip installation
``` bash
pip install . -v
```
> **TIPS :** Use environment variables to change compilation options :
> - `AIDGE_INSTALL` : to set the installation folder. Defaults to `<python_prefix>/lib/libAidge`. :warning: This path must be identical to aidge_core install path.
> - `AIDGE_PYTHON_BUILD_TYPE` : to set the compilation mode to **Debug** or **Release** or "" (for default flags). Defaults to **Release**.
> - `AIDGE_BUILD_GEN` : to set the build backend (for development mode) or "" for the cmake default. Default to "".

## Pip installation for development

To setup using pip in development (or editable mode), use the `--no-build-isolation -e` options to pip.

For instance run the following command in your python environnement for a typical setup :
``` bash
export AIDGE_PYTHON_BUILD_TYPE=         # default flags (no debug info but fastest build time)
export AIDGE_PYTHON_BUILD_TYPE=Debug    # or if one really need to debug the C++ code
pip install -U pip setuptools setuptools_scm[toml] cmake   # Pre-install build requirements (refer to the pyproject.toml [build-system] section)
pip install -v --no-build-isolation -e .
```

Refer to `aidge_core/README.md` for more details on development build options.

### Standard C++ Compilation

You will need to compile and install the [Core Library](https://gitlab.eclipse.org/eclipse/aidge/aidge_core) before compiling the CPU one.

Once this has been done, you'll need run CMake with the
`CMAKE_INSTALL_PREFIX:PATH` flag, in order to indicate to CMake where
`aidge_core` has been installed : 
```sh
cmake -DCMAKE_INSTALL_PREFIX:PATH=$(path_to_install_folder) $(CMAKE PARAMETERS) $(projet_root)

make all
```

More detailed information is available in the [Aidge User Guide](https://eclipse.dev/aidge/source/GetStarted/install.html)
